# MAZE GAME #

Java implementation of a maze game.

It allows to generate random mazes, with different sizes and number of dragons. It also allows the user to create mazes the way he wants and to save a game in order to play later from where he left the game.