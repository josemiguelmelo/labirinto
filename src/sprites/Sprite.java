/**
 * @author José Miguel Melo, ei12050; Ricardo Loureiro, ei12034
 */

package sprites;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;


/**
 * The Class Sprite.
 * Used for the class game in order to use sprites.
 */
public class Sprite extends JPanel{
	
	/** The sprite. */
	BufferedImage sprite;
	
	/** The bg. */
	Image bg = null;
	
	/** The row. */
	int col, row;
	
	/** The height. */
	int width , height;
	
	/** The sprites. */
	BufferedImage[] sprites;
	
	/** The i. */
	int i=0;

	/** The posicaox. */
	int posicaox = 0;
	
	/** The posicaoy. */
	int posicaoy = 0;
	
	
	/**
	 * Instantiates a new sprite.
	 *
	 * @param sprite the sprite
	 * @param col the col
	 * @param row the row
	 */
	public Sprite(BufferedImage sprite, int col, int row){
		this.sprite=sprite;
		this.col=col;
		this.row = row;
		
		this.width = sprite.getWidth();
		this.height = sprite.getHeight();
		
		this.sprites  = new BufferedImage[row * col];
		init();
	}
	
	/**
	 * Inits the sprite.
	 */
	public void init(){
		int x=0;
		int y=0;
		int q = 0;
		for (int i = 0; i < row; i++)
		{
		    for (int j = 0; j < col; j++)
		    {
		        sprites[q] = sprite.getSubimage(
		        		x,
			            y,
			            width/col, 
			            height/row
		        );
		      x+=(width/col);
		        q++;
		    }
		    y+=(height/row);
		    x=0;
		}
	}
	
	/**
	 * Adds the background.
	 *
	 * @param bg the bg
	 */
	public void addBackground(Image bg){
		this.bg = bg;
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	public void paintComponent(Graphics g){
		if(i==sprites.length)
			i=0;
		if(bg != null)
			g.drawImage(bg, posicaox, posicaoy, super.getWidth(), super.getHeight(),null);
		
		g.drawImage(sprites[i], posicaox, posicaoy, super.getWidth(), super.getHeight(),null);
		i++;
	}
}

