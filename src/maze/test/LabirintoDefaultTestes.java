/**
 * @author José Miguel Melo, ei12050; Ricardo Loureiro, ei12034
 */

package maze.test;

import static org.junit.Assert.*;
import org.junit.Test;

import maze.logic.*;
import maze.logic.Tabuleiro.TabuleiroBuilder;

// TODO: Auto-generated Javadoc
/**
 * The Class LabirintoDefaultTestes.
 */
public class LabirintoDefaultTestes {
	
	/**
	 * Parado heroi.
	 */
	@Test
	public void paradoHeroi(){
		TabuleiroBuilder builder = new TabuleiroBuilder();
		Tabuleiro tab = builder.build();
		tab.move(Constants.CIMA);

		Point heroiPosicao = new Point(tab.h.getXPosicao(), tab.h.getYPosicao(), null);
		Point posicao = new Point(1,1, null);

		assertEquals(posicao, heroiPosicao);
		
		tab.move(Constants.BAIXO);
		tab.move(Constants.DIREITA);// move contra a parede

		heroiPosicao.x = tab.h.getXPosicao();
		heroiPosicao.y = tab.h.getYPosicao();
		posicao.y = 2;
		assertEquals(posicao, heroiPosicao);
	}
	
	/**
	 * Mover heroi.
	 */
	@Test 
	public void moverHeroi(){

		TabuleiroBuilder builder = new TabuleiroBuilder();
		Tabuleiro tab = builder.build();
		tab.move(Constants.BAIXO);
		Point expected = new Point(1,2,null);
		Point ponto = new Point(tab.h.getXPosicao(), tab.h.getYPosicao(), null);
		assertEquals(expected, ponto);
	}
	
	/**
	 * Apanha espada.
	 *
	 * @param tab the tab
	 */
	void apanhaEspada(Tabuleiro tab){
		while(tab.h.getYPosicao() < tab.espada.getYPosicao())
			tab.move(Constants.BAIXO);
	}
	
	/**
	 * Apanhar espada.
	 */
	@Test
	public void apanharEspada(){
		TabuleiroBuilder builder = new TabuleiroBuilder();
		Tabuleiro tab = builder.build();
		
		apanhaEspada(tab);
		
		assertEquals(Constants.hArmadoRep, tab.h.representacao);
		assertEquals(true, tab.h.getArmado());
		int x = tab.h.getXPosicao();
		int y = tab.h.getYPosicao();
		tab.move(Constants.CIMA);
		assertEquals(Constants.hArmadoRep, tab.h.representacao);
		assertEquals(Constants.EspacoRep, tab.getTabuleiro()[y][x]);	// testa se a espada nao ficou no tabuleiro depois de o heroi a apanhar e se mover
	}

	/**
	 * Morto dragao.
	 */
	@Test
	public void mortoDragao(){
		TabuleiroBuilder builder = new TabuleiroBuilder();
		Tabuleiro tab = builder.build();
		
		
		int[] moves = {Constants.BAIXO, Constants.BAIXO, Constants.BAIXO, Constants.BAIXO, Constants.DIREITA};
		int i=0;
		while(i < moves.length){
			tab.move(moves[i]);
			tab.morre();
			i++;
		}
		
		assertEquals(true, tab.morre());
		assertEquals(true, tab.h.getMorto());
		
	}

	/**
	 * Matar dragao.
	 */
	@Test
	public void matarDragao(){
		TabuleiroBuilder builder = new TabuleiroBuilder();
		Tabuleiro tab = builder.build();
		
		apanhaEspada(tab);
		
		while(tab.h.getYPosicao() != tab.getDragoes().get(0).getYPosicao() ){
			tab.mataDragao();
			tab.move(Constants.CIMA);
		}
		
		assertEquals(Constants.hArmadoRep, tab.h.representacao);
		
		// antes de matar o dragao
		assertEquals(1, tab.getDragoes().size());
		
		tab.move(Constants.DIREITA);	// move para junto do dragao
		assertEquals(false, tab.morre());
		assertEquals(true, tab.verificaDragao());
		tab.mataDragao();
		// depois de matar o dragao, vai haver menos um dragao na lista dos dragoes
		assertEquals(0, tab.getDragoes().size());
		
	}

	/**
	 * Ganhar jogo.
	 */
	@Test
	public void ganharJogo(){
		TabuleiroBuilder builder = new TabuleiroBuilder();
		Tabuleiro tab = builder.build();
		
		apanhaEspada(tab);
		
		
		int[] moves = { Constants.CIMA, Constants.CIMA, Constants.CIMA, Constants.DIREITA, Constants.DIREITA, Constants.DIREITA, 
				Constants.DIREITA, Constants.DIREITA, Constants.BAIXO, Constants.BAIXO, Constants.BAIXO, Constants.DIREITA, Constants.DIREITA,
				Constants.CIMA, Constants.CIMA, Constants.CIMA, Constants.DIREITA};
		
		assertEquals(1, tab.getDragoes().size());
		assertEquals(false, tab.fimJogo());
		
		for(int i = 0; i < moves.length; i++){
			tab.move(moves[i]);
			tab.mataDragao();
		}
		assertEquals(0, tab.getDragoes().size());
		assertEquals(true, tab.fimJogo());
	}

	/**
	 * Alcancar saida sem ganhar.
	 */
	@Test
	public void alcancarSaidaSemGanhar(){
		TabuleiroBuilder builder = new TabuleiroBuilder();
		Tabuleiro tab = builder.build();
		
		int[] moves = { Constants.DIREITA, Constants.DIREITA, Constants.DIREITA, Constants.DIREITA, Constants.DIREITA, Constants.DIREITA, Constants.DIREITA, 
				Constants.BAIXO, Constants.BAIXO, Constants.BAIXO, Constants.BAIXO, Constants.DIREITA};
		
		assertEquals(1, tab.getDragoes().size());
		assertEquals(false, tab.fimJogo());
		
		for(int i = 0; i < moves.length; i++){
			tab.move(moves[i]);
		}
		
		Point expected = new Point(8,5, null);
		Point obtained = new Point(tab.h.getXPosicao(), tab.h.getYPosicao(), null);
		
		assertEquals(expected, obtained);	// posicao junto a saida
		assertEquals(1, tab.getDragoes().size());	// nao matou o dragao
		assertEquals(false, tab.fimJogo());	// nao acaba o jogo
	}

}
