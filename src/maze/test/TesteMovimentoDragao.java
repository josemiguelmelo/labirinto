/**
 * @author José Miguel Melo, ei12050; Ricardo Loureiro, ei12034
 */

package maze.test;

import static org.junit.Assert.*;
import maze.logic.Constants;
import maze.logic.Dragao;
import maze.logic.MyRandom;
import maze.logic.Point;
import maze.logic.Tabuleiro;
import maze.logic.Tabuleiro.TabuleiroBuilder;

import org.junit.Test;

// TODO: Auto-generated Javadoc
/**
 * The Class TesteMovimentoDragao.
 */
public class TesteMovimentoDragao {

	/**
	 * Mover dragao.
	 */
	@Test
	public void moverDragao() {

		int [] direcoes = {Constants.ESQUERDA, Constants.ESQUERDA, Constants.BAIXO, Constants.BAIXO};
		TabuleiroBuilder builder = new TabuleiroBuilder();
		builder.random(new MyRandom(direcoes));
		Tabuleiro tab = builder.build();

		Point expected = new Point(1,7,null);
		for(int i=0; i < direcoes.length; i++)
			tab.moverDragao();

		Point pontoObtido = new Point(tab.getDragoes().get(0).getXPosicao(), tab.getDragoes().get(0).getYPosicao(), null);
		assertEquals(expected, pontoObtido);
	}

	/**
	 * Dragao com espada.
	 */
	@Test
	public void dragaoComEspada(){
		int [] direcoes = {Constants.ESQUERDA, Constants.ESQUERDA, Constants.BAIXO, Constants.BAIXO, Constants.BAIXO};
		TabuleiroBuilder builder = new TabuleiroBuilder();
		builder.random(new MyRandom(direcoes));
		Tabuleiro tab = builder.build();

		Point expected = new Point(1,8,null);
		for(int i=0; i < direcoes.length; i++)
			tab.moverDragao();

		Point pontoObtido = new Point(tab.getDragoes().get(0).getXPosicao(), tab.getDragoes().get(0).getYPosicao(), null);
		assertEquals(expected, pontoObtido);
		
		// verifica se a representacao do dragao com a espada esta correta
		assertEquals(Constants.DragaoEspadaRep, tab.getDragoes().get(0).representacao);
	}

	/**
	 * Dragao dormir.
	 */
	@Test
	public void dragaoDormir(){
		int [] direcoes = {Constants.ESQUERDA, Constants.ESQUERDA, Constants.BAIXO, Constants.BAIXO, Constants.BAIXO, Constants.DIREITA};
		TabuleiroBuilder builder = new TabuleiroBuilder();
		builder.random(new MyRandom(direcoes));
		Tabuleiro tab = builder.build();
		
		// ponto esperado corresponde ao inicial, pois o dragao vai estar a dormir, logo nao se move
		Point expected = new Point(tab.getDragoes().get(0).getXPosicao(),tab.getDragoes().get(0).getYPosicao(),null);
		// coloca o dragao a dormir
		tab.getDragoes().get(0).setDormir(true);
		for(int i=0; i < direcoes.length; i++)
			tab.moverDragao();
		// nao foi efetuado nenhum movimento. o dragao estava a dormir
		Point obtained = new Point(tab.getDragoes().get(0).getXPosicao(),tab.getDragoes().get(0).getYPosicao(),null);
		assertEquals(expected, obtained);
		
		
		// altera o dragao para nao dormir e, assim, poder mover
		tab.getDragoes().get(0).setDormir(false);
		// valor esperado e o dragao andar para a esquerda, pois nao foi efetuado nenhum movimento ate agora
		Point expected1 = new Point(tab.getDragoes().get(0).getXPosicao()-1,tab.getDragoes().get(0).getYPosicao(),null);
		tab.moverDragao();
		// valor obtido depois do dragao mover
		Point obtained1 = new Point(tab.getDragoes().get(0).getXPosicao(),tab.getDragoes().get(0).getYPosicao(),null);
		assertEquals(expected1, obtained1);
	}

	/**
	 * Adicionar dragao.
	 */
	@Test
	public void adicionarDragao(){
		TabuleiroBuilder builder = new TabuleiroBuilder();
		Tabuleiro tab = builder.build();
		
		assertEquals(1, tab.getDragoes().size());
		tab.inserirDragao(new Dragao());
		assertEquals(2, tab.getDragoes().size());
		

		tab.inserirDragao(new Dragao());
		tab.inserirDragao(new Dragao());
		tab.inserirDragao(new Dragao());
		assertEquals(5, tab.getDragoes().size());
	}

	/**
	 * Dragao dormir morre.
	 */
	@Test
	public void dragaoDormirMorre(){
		TabuleiroBuilder builder = new TabuleiroBuilder();
		Tabuleiro tab = builder.build();
		
		tab.getDragoes().get(0).setDormir(true);
		
		int[] direcoes = { Constants.BAIXO, Constants.BAIXO, Constants.BAIXO, Constants.BAIXO, 
							Constants.DIREITA, Constants.DIREITA };
		// coloca o heroi a beira do dragao
		for(int i =0; i < direcoes.length;i++)
			tab.move(direcoes[i]);
		
		// dragao a dormir. heroi nao morre
		assertEquals(false, tab.morre());
		
		
		tab.getDragoes().get(0).setDormir(false);
		// dragao nao esta a dormir. heroi morre
		assertEquals(true, tab.morre());
		
	}

}
