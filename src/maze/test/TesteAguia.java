/**
 * @author José Miguel Melo, ei12050; Ricardo Loureiro, ei12034
 */

package maze.test;

import static org.junit.Assert.*;
import maze.logic.Constants;
import maze.logic.MyRandom;
import maze.logic.Point;
import maze.logic.Tabuleiro;
import maze.logic.Tabuleiro.TabuleiroBuilder;

import org.junit.Test;

// TODO: Auto-generated Javadoc
/**
 * The Class TesteAguia.
 */
public class TesteAguia {

	// 
	/**
	 * Testa se a aguia e lancada e se chega a espada.
	 */
	@Test
	public void lancarAguia() {
		TabuleiroBuilder builder = new TabuleiroBuilder();
		Tabuleiro tab = builder.build();
		
		int[] direcoes = { Constants.lancaAGUIA, Constants.BAIXO, Constants.CIMA, Constants.BAIXO, Constants.CIMA, Constants.BAIXO, Constants.CIMA,
				Constants.BAIXO, Constants.CIMA };
		
		for(int i = 0 ; i < direcoes.length; i++){
			tab.move(direcoes[i]);
		}
		
		Point expected = new Point(1, 8, null);
		Point obtained = new Point(tab.getAguia().getXPosicao(), tab.getAguia().getYPosicao(), null);
		
		// aguia tem que estar na posicao (1,8)
		assertEquals(expected, obtained);
		// aguia esta em cima da espada. representacao tem de ser igual a AguiaEspadaRep
		assertEquals(Constants.AguiaEspadaRep, tab.getAguia().representacao);
	}

	// 
	/**
	 * Testa se a espada foi transportada ate a posicao inicial e se o heroi consegue apanha-la.
	 */
	@Test
	public void tranportaEspadaPosicaoInicial(){
		TabuleiroBuilder builder = new TabuleiroBuilder();
		Tabuleiro tab = builder.build();
		
		int[] direcoes = { Constants.lancaAGUIA, Constants.BAIXO, Constants.CIMA, Constants.BAIXO, Constants.CIMA, Constants.BAIXO, Constants.CIMA,
				Constants.BAIXO, Constants.CIMA, Constants.DIREITA, Constants.DIREITA, Constants.ESQUERDA, Constants.DIREITA, Constants.ESQUERDA,
				Constants.DIREITA, Constants.ESQUERDA, Constants.ESQUERDA };
		
		for(int i = 0 ; i < direcoes.length; i++){
			tab.move(direcoes[i]);
		}
		
		Point expected = new Point(1, 1, null);
		Point obtained = new Point(tab.getAguia().getXPosicao(), tab.getAguia().getYPosicao(), null);
		
		// aguia tem que estar na posicao (1,1)
		assertEquals(expected, obtained);
		// heroi esta em cima da espada. heroi armado
		assertEquals(true, tab.h.getArmado());
		assertEquals(Constants.hArmadoRep, tab.h.representacao);
	}

	// 
	/**
	 * Testa se a aguia morre ao poisar quando vai buscar a espada.
	 */
	@Test
	public void aguiaMorreBuscarEspada(){
		TabuleiroBuilder builder = new TabuleiroBuilder();
		int[] direcoesDragao = {Constants.ESQUERDA, Constants.ESQUERDA, Constants.BAIXO, Constants.BAIXO};
		builder.random(new MyRandom(direcoesDragao));
		Tabuleiro tab = builder.build();
		
		int[] direcoes = { Constants.lancaAGUIA, Constants.BAIXO, Constants.CIMA, Constants.BAIXO, Constants.CIMA, Constants.BAIXO, Constants.CIMA,
				Constants.BAIXO, Constants.CIMA, Constants.DIREITA, Constants.ESQUERDA, Constants.DIREITA};
		
		for(int i = 0 ; i < direcoes.length; i++){
			tab.move(direcoes[i]);
			if(i<direcoesDragao.length)
				tab.moverDragao();
		}

		Point expected = new Point(1,8, null);
		Point obtained = new Point(tab.getAguia().getXPosicao(), tab.getAguia().getYPosicao(), null);
		assertEquals(true, tab.getAguia().getMorta());	// aguia morre quando poisa
		assertEquals(expected, obtained);	// aguia tem que estar na posicao da espada, pois morre la
		
	}

}
