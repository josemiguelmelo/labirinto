/**
 * @author José Miguel Melo, ei12050; Ricardo Loureiro, ei12034
 */

package maze.gui;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.Timer;

import maze.logic.Aguia;
import maze.logic.Tabuleiro;

/**
 * The Class GUI.
 * It's the main JFrame that the user will be seeing during the entire game.
 */
public class GUI extends JFrame {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The content pane. */
	public JPanel contentPane;
	
	/** The panel. */
	public Game panel;
	
	/** The menu panel. */
	public JPanel menuPanel;
	
	/** The card layout. */
	public CardLayout cardLayout ;

	/**
	 * Create the frame.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public GUI() throws IOException {
		setBounds(100, 100, 631, 521);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnMainMenu = new JMenu("Main Menu");
		mnMainMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
				new MainMenu().setVisible(true);
			}
		});
		menuBar.add(mnMainMenu);

		JMenuItem mntmNewGame = new JMenuItem("New Game");
		mntmNewGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
				try {		
					GUI table =	new GUI();
					table.setVisible(true);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		mnMainMenu.add(mntmNewGame);

		JMenuItem mntmGoToMain = new JMenuItem("Go to Main Menu");
		mntmGoToMain.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				new MainMenu().setVisible(true);
			}
		});
		mnMainMenu.add(mntmGoToMain);

		JMenu mnOptions = new JMenu("Options");
		menuBar.add(mnOptions);

		JMenuItem mntmKeyboard = new JMenuItem("Keyboard");
		mntmKeyboard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyboardOptions keyboardOption = new KeyboardOptions();
				keyboardOption.setVisible(true);
			}
		});
		mnOptions.add(mntmKeyboard);

		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Maze Dimension");
		mntmNewMenuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MazeOptions mazeOptions = new MazeOptions();
				mazeOptions.setVisible(true);
			}
		});
		mnOptions.add(mntmNewMenuItem_2);

		JMenuItem mntmCloseGame = new JMenuItem("Close Game");
		mntmCloseGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				System.exit(0);
			}
		});
		
		JMenuItem mntmSaveMaze = new JMenuItem("Save Maze");
		mntmSaveMaze.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File saves = new File("Save");
				if(!saves.exists()){
					saves.mkdir();
				}
				Tabuleiro tab = panel.getTab();
				ObjectOutputStream os = null; 
				String timeStamp = new SimpleDateFormat("HH-mm-ss").format(Calendar.getInstance().getTime());
				Calendar cal = Calendar.getInstance();
				cal.getTime();
				try {
					os = new ObjectOutputStream( 
							new FileOutputStream(new File(".").getCanonicalFile() + File.separator +"Save"+File.separator + "Maze" + " - "+ timeStamp + ".dat"));
					} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} 
				try {
					os.writeObject(tab);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} 


				if (os != null)
					try {
						os.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

			}
		});
		menuBar.add(mntmSaveMaze);
		mntmCloseGame.setBackground(Color.WHITE);
		menuBar.add(mntmCloseGame);


		getContentPane().removeAll();
		getContentPane().setLayout(new CardLayout(0, 0));


		panel=new Game(this);
		getContentPane().add(panel, "Game");


		//validate();
		Timer time = new Timer(50, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				repaint();				
			}
		});
		time.start();
	}

	/**
	 * Instantiates a new gui.
	 *
	 * @param tab the tab
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public GUI(final char[][] tab) throws IOException {
		setBounds(100, 100, 631, 521);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnMainMenu = new JMenu("Main Menu");
		mnMainMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
				new MainMenu().setVisible(true);
			}
		});
		menuBar.add(mnMainMenu);

		JMenuItem mntmNewGame = new JMenuItem("New Game");
		mntmNewGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
				try {		
					GUI table =	new GUI();
					table.setVisible(true);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		mnMainMenu.add(mntmNewGame);

		JMenuItem mntmGoToMain = new JMenuItem("Go to Main Menu");
		mntmGoToMain.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				new MainMenu().setVisible(true);
			}
		});
		mnMainMenu.add(mntmGoToMain);

		JMenu mnOptions = new JMenu("Options");
		menuBar.add(mnOptions);

		JMenuItem mntmKeyboard = new JMenuItem("Keyboard");
		mntmKeyboard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyboardOptions keyboardOption = new KeyboardOptions();
				keyboardOption.setVisible(true);
			}
		});
		mnOptions.add(mntmKeyboard);

		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Maze Dimension");
		mntmNewMenuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MazeOptions mazeOptions = new MazeOptions();
				mazeOptions.setVisible(true);
			}
		});
		mnOptions.add(mntmNewMenuItem_2);

		JMenuItem mntmCloseGame = new JMenuItem("Close Game");
		mntmCloseGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				System.exit(0);
			}
		});


		JMenuItem mntmSaveMaze = new JMenuItem("Save Maze");
		mntmSaveMaze.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File saves = new File("Save");
				if(!saves.exists()){
					saves.mkdir();
				}
				Tabuleiro tab = panel.getTab();
				ObjectOutputStream os = null; 
				String timeStamp = new SimpleDateFormat("HH-mm-ss").format(Calendar.getInstance().getTime());
				Calendar cal = Calendar.getInstance();
				cal.getTime();
				try {
					os = new ObjectOutputStream( 
							new FileOutputStream(new File(".").getCanonicalFile() + File.separator +"Save"+File.separator + "Maze" + " - "+ timeStamp + ".dat"));
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} 
				try {
					os.writeObject(tab);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} 


				if (os != null)
					try {
						os.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

			}
		});
		menuBar.add(mntmSaveMaze);
		mntmCloseGame.setBackground(Color.WHITE);
		menuBar.add(mntmCloseGame);


		getContentPane().removeAll();
		getContentPane().setLayout(new CardLayout(0, 0));


		panel=new Game(this,tab);
		getContentPane().add(panel, "Game");


		//validate();
		Timer time = new Timer(50, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				repaint();				
			}
		});
		time.start();
	}

	/**
	 * Instantiates a new gui.
	 *
	 * @param tab the tab
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public GUI(Tabuleiro tab) throws IOException {
		setBounds(100, 100, 631, 521);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnMainMenu = new JMenu("Main Menu");
		mnMainMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
				new MainMenu().setVisible(true);
			}
		});
		menuBar.add(mnMainMenu);

		JMenuItem mntmNewGame = new JMenuItem("New Game");
		mntmNewGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
				try {		
					GUI table =	new GUI();
					table.setVisible(true);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		mnMainMenu.add(mntmNewGame);

		JMenuItem mntmGoToMain = new JMenuItem("Go to Main Menu");
		mntmGoToMain.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				new MainMenu().setVisible(true);
			}
		});
		mnMainMenu.add(mntmGoToMain);

		JMenu mnOptions = new JMenu("Options");
		menuBar.add(mnOptions);

		JMenuItem mntmKeyboard = new JMenuItem("Keyboard");
		mntmKeyboard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyboardOptions keyboardOption = new KeyboardOptions();
				keyboardOption.setVisible(true);
			}
		});
		mnOptions.add(mntmKeyboard);

		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Maze Dimension");
		mntmNewMenuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MazeOptions mazeOptions = new MazeOptions();
				mazeOptions.setVisible(true);
			}
		});
		mnOptions.add(mntmNewMenuItem_2);

		JMenuItem mntmCloseGame = new JMenuItem("Close Game");
		mntmCloseGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				System.exit(0);
			}
		});


		JMenuItem mntmSaveMaze = new JMenuItem("Save Maze");
		mntmSaveMaze.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File saves = new File("Save");
				if(!saves.exists()){
					saves.mkdir();
				}
				Tabuleiro tab = panel.getTab();
				ObjectOutputStream os = null; 
				String timeStamp = new SimpleDateFormat("HH-mm-ss").format(Calendar.getInstance().getTime());
				Calendar cal = Calendar.getInstance();
				cal.getTime();
				try {
					os = new ObjectOutputStream( 
							new FileOutputStream(new File(".").getCanonicalFile() + File.separator +"Save"+File.separator + "Maze" + " - "+ timeStamp + ".dat"));
					} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} 
				try {
					os.writeObject(tab);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} 


				if (os != null)
					try {
						os.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

			}
		});
		menuBar.add(mntmSaveMaze);
		mntmCloseGame.setBackground(Color.WHITE);
		menuBar.add(mntmCloseGame);


		getContentPane().removeAll();
		getContentPane().setLayout(new CardLayout(0, 0));


		panel=new Game(this,tab);
		getContentPane().add(panel, "Game");


		//validate();
		Timer time = new Timer(50, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				repaint();				
			}
		});
		time.start();
	}
}
