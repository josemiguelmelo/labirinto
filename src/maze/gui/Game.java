/**
 * @author José Miguel Melo, ei12050; Ricardo Loureiro, ei12034
 */

package maze.gui;

import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

import maze.cli.UserInterface;
import maze.logic.Constants;
import maze.logic.Tabuleiro;
import maze.logic.Tabuleiro.TabuleiroBuilder;
import sprites.Sprite;


/**
 * The Class Game.
 * This class represents the main panel that the playing board will be displayed on.

 */
@SuppressWarnings("serial")
public class Game extends JPanel implements KeyListener{

	/** The tab. */
	public Tabuleiro tab;

	/** The escape. */
	static boolean escape = false;

	/** The end game. */
	boolean endGame;

	/** The frame. */
	JFrame frame;

	/** The wall. */
	ImageIcon wall = new ImageIcon("images/Texture_wall.png");

	/** The grass. */
	ImageIcon grass = new ImageIcon("images/14.jpg");

	/** The hero. */
	ImageIcon hero = new ImageIcon("images/unnamed.png");

	/** The dragon. */
	ImageIcon dragon = new ImageIcon("images/dragon.gif");

	/** The sleep dragon. */
	ImageIcon sleepDragon = new ImageIcon("images/sleepDragon.gif");

	/** The eagle. */
	ImageIcon eagle = new ImageIcon("eagle.gif");

	/** The sword. */
	ImageIcon sword = new ImageIcon("images/sword.png");

	/** The exit. */
	ImageIcon exit = new ImageIcon("images/door.JPG");

	/** The dead hero. */
	ImageIcon deadHero = new ImageIcon("images/dead_link.png");

	/** The hero sword. */
	ImageIcon heroSword = new ImageIcon("images/armado.png");

	/** The white. */
	ImageIcon white = new ImageIcon ("images/square.jpg");

	/** The eagle sprite. */
	Sprite eagleSprite;

	/** The dimension. */
	static int dimension = 10;

	/** The random. */
	static int random = Tabuleiro.DEFAULT;

	/** The num dragons. */
	static int numDragons = 1;

	/** The type. */
	static int type = 3;
	// type = 1 -> static
	// type = 2 -> mover
	// type = 3 -> mover e dormir

	/** The up key. */
	static int UP_KEY = KeyEvent.VK_W;

	/** The down key. */
	static int DOWN_KEY = KeyEvent.VK_S;

	/** The left key. */
	static int LEFT_KEY = KeyEvent.VK_A;

	/** The right key. */
	static int RIGHT_KEY = KeyEvent.VK_D;

	/** The eagle key. */
	static int EAGLE_KEY = KeyEvent.VK_E;


	/**
	 * Instantiates a new game panel.
	 *
	 * @param frame the frame
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public Game(JFrame frame) throws IOException{
		this.frame = frame;
		BufferedImage image = ImageIO.read(new File("images/eagleLeftSprite.png"));
		eagleSprite = new Sprite(image, 4,1);

		TabuleiroBuilder builder;
		if(random == Tabuleiro.DEFAULT){
			builder = new TabuleiroBuilder();
			setLayout(new GridLayout(10, 10));
		}
		else{
			builder = new TabuleiroBuilder(random, dimension, numDragons);
			setLayout(new GridLayout(dimension, dimension));
		}
		this.tab = builder.build();
		setFocusable(true);
		endGame = false;
		addKeyListener(this);
		show();
	}

	/**
	 * Instantiates a new game panel.
	 * Overload constructor used from Create Maze mode
	 * 
	 * @param frame the frame
	 * @param tab2 the tab2
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public Game(JFrame frame, char[][] tab2) throws IOException {
		this.frame = frame;
		BufferedImage image = ImageIO.read(new File("images/eagleLeftSprite.png"));
		eagleSprite = new Sprite(image, 4,1);

		TabuleiroBuilder builder;
		builder = new TabuleiroBuilder(tab2);
		setLayout(new GridLayout(tab2.length,tab2.length));

		tab = builder.build();
		setFocusable(true);
		endGame = false;
		addKeyListener(this);
		show();
	}

	/**
	 * Instantiates a new game.
	 * Overload constructor used from load board.
	 * @param frame the frame
	 * @param tab2 the tab2
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public Game(JFrame frame, Tabuleiro tab2) throws IOException {
		this.frame = frame;
		BufferedImage image = ImageIO.read(new File("images/eagleLeftSprite.png"));
		eagleSprite = new Sprite(image, 4,1);
		UserInterface.mostraTabuleiro(tab2);
		this.tab = tab2;
		setLayout(new GridLayout(tab.getTabuleiro().length,tab.getTabuleiro().length));
		setFocusable(true);
		endGame = false;
		addKeyListener(this);
		show();
	}


	/**
	 * Will change and then show the panels that are currently displaying the board
	 */
	@Override
	public void show(){
		removeAll();
		for(int i = 0; i < tab.getTabuleiro().length; i++){
			for(int j = 0; j < tab.getTabuleiro().length ; j++){
				MazeImage ja = null;
				//PAREDE
				if(tab.getTabuleiro()[i][j] == 'K') {
					ja = new MazeImage(wall.getImage());
					add(ja);
				}
				else if(tab.getTabuleiro()[i][j] == Constants.ParedeRep){
					ja = new MazeImage(wall.getImage());
					add(ja);
				}
				//CAMINHO
				else if(tab.getTabuleiro()[i][j] == Constants.EspacoRep){
					ja = new MazeImage(grass.getImage());
					add(ja);
				}
				//HEROI
				else if(tab.getTabuleiro()[i][j]==Constants.hMorto){
					ja = new MazeImage(deadHero.getImage());
					ja.addBackground(grass.getImage());
					add(ja);
				}	
				else if(tab.getTabuleiro()[i][j] == Constants.hRepresentacao){
					ja = new MazeImage(hero.getImage());
					ja.addBackground(grass.getImage());
					add(ja);
				}else if(tab.getTabuleiro()[i][j] == Constants.AguiaHeroiRep){
					ja = new MazeImage(hero.getImage());
					ja.addBackground(grass.getImage());
					add(ja);
				}
				else if(tab.getTabuleiro()[i][j] == Constants.DragaoRep){
					ja = new MazeImage(dragon.getImage());
					ja.addBackground(grass.getImage());
					add(ja);
				}
				else if(tab.getTabuleiro()[i][j] == Constants.DragaoAdRep){
					ja = new MazeImage(sleepDragon.getImage());
					ja.addBackground(grass.getImage());
					add(ja);
				}
				else if(tab.getTabuleiro()[i][j] == Constants.DragaoEspadaRep){
					ja = new MazeImage(dragon.getImage());
					ja.addBackground(grass.getImage());
					add(ja);
				}
				else if(tab.getTabuleiro()[i][j] == Constants.EspadaRep){
					ja = new MazeImage(sword.getImage());
					ja.addBackground(grass.getImage());
					add(ja);
				}
				else if(tab.getTabuleiro()[i][j] == Constants.SaidaRep){
					ja = new MazeImage(exit.getImage());
					add(ja);
				}
				else if(tab.getTabuleiro()[i][j] == Constants.hArmadoRep){
					ja = new MazeImage(heroSword.getImage());
					ja.addBackground(grass.getImage());
					add(ja);
				}
				else if(tab.getTabuleiro()[i][j] == Constants.AguiaRep || 
						tab.getTabuleiro()[i][j] == Constants.AguiaEspadaRep || 
						tab.getTabuleiro()[i][j] == Constants.AguiaParede){
					if(tab.getTabuleiro()[i][j] == Constants.AguiaParede)
						eagleSprite.addBackground(wall.getImage());
					else
						eagleSprite.addBackground(grass.getImage());
					add(eagleSprite);
				}
			}
		}
		validate();
	}

	/**
	 * Returns the current Board
	 *
	 * @return the tab
	 */
	public Tabuleiro getTab() {
		return tab;
	}


	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
	}

	/**
	 * Event handler, allows the user to move the hero, changing the current game board
	 */
	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub	
		boolean moved = false;
		if(e.getKeyCode() == LEFT_KEY){
			tab.move(Constants.ESQUERDA);
			moved=true;
		}
		else if(e.getKeyCode() == DOWN_KEY){
			tab.move(Constants.BAIXO);
			moved=true;
		}else if(e.getKeyCode() == RIGHT_KEY){
			tab.move(Constants.DIREITA);moved=true;
		}else if(e.getKeyCode() == UP_KEY){
			tab.move(Constants.CIMA);moved=true;
		}else if(e.getKeyCode() == EAGLE_KEY){
			tab.move(Constants.lancaAGUIA);
		}else if(e.getKeyCode() == KeyEvent.VK_ESCAPE){
			escape=true;
		}

		if(moved){
			if(type!=1){
				if(type==3){
					tab.dragaoDormir();	// coloca alguns dragoes a dormir
				}
				tab.moverDragao();
			}
			if(tab.verificaDragao() && !tab.morre()){
				tab.mataDragao();
			}
		}
		if(tab.fimJogo()){
			endGame=true;
			show();
			removeKeyListener(this);
			WonGame wonGame;
			LostGame lostGame;	
			if(!tab.morre()){
				wonGame = new WonGame(frame);
				wonGame.setVisible(true);
			}
			else {
				lostGame = new LostGame(frame); 
				lostGame.setVisible(true);
			}
		}
		else
			show();
	}

	/* (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub


	}

}




