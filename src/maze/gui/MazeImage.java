/**
 * @author José Miguel Melo, ei12050; Ricardo Loureiro, ei12034
 */

package maze.gui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

/**
 * The Class MazeImage.
 * It's a Jpanel that will be used in the class Game.
 */
public class MazeImage extends JPanel{

	/** The img. */
	private Image img; 
	
	/** The bg img. */
	private Image bgImg;
	
    /**
     * Instantiates a new maze image.
     *
     * @param img the img
     */
    public MazeImage(Image img) {
      this.img = img;
      this.bgImg = null;
      Dimension size = new Dimension(super.getWidth(), super.getHeight());
     // Dimension size = new Dimension(img.getWidth(null), img.getHeight(null));
      setPreferredSize(size);
      setMinimumSize(size);
      setMaximumSize(size);
      setSize(size);
      setLayout(null);
    }

    /**
     * Adds the background.
     *
     * @param image the image that will be add.
     */
    public void addBackground(Image img){
    	bgImg = img;
    }
    
    /* (non-Javadoc)
     * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
     */
    public void paintComponent(Graphics g) {
    	if(bgImg != null)
    		g.drawImage(bgImg, 0, 0, super.getWidth(), super.getHeight(), null);
        		
    	g.drawImage(img, 0, 0, super.getWidth(), super.getHeight(), null);
    }

}
