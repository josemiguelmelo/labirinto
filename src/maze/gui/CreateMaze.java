/**
 * @author José Miguel Melo, ei12050; Ricardo Loureiro, ei12034
 */



package maze.gui;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;

import maze.logic.Constants;
import maze.logic.Tabuleiro;


/**
 * The Class CreateMaze.
 * Allows the user to create their own customized Maze with all the functions and possibilities.	
 * 
 */
public class CreateMaze extends JFrame{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The tab. */
	char[][] tab;
	
	/** The combo box_1. */
	JComboBox comboBox_1;
	
	/** The combo box. */
	JComboBox comboBox;
	
	/** The panels. */
	ArrayList<JPanel> panels = new ArrayList<JPanel> ();
	
	/** The panel. */
	JPanel panel;
	
	/** The btn done. */
	final JButton btnDone;
	
	/** The panel_1. */
	JPanel panel_1;
	
	/** The panel_2. */
	JPanel panel_2;
	
	/** The wall. */
	ImageIcon wall = new ImageIcon("images"+ File.separator +"Texture_wall.png");
	
	/** The grass. */
	ImageIcon grass = new ImageIcon("images"+ File.separator +"14.jpg");
	
	/** The hero. */
	ImageIcon hero = new ImageIcon("images"+ File.separator +"unnamed.png");
	
	/** The dragon. */
	ImageIcon dragon = new ImageIcon("images"+ File.separator +"dragon.gif");
	
	/** The sleep dragon. */
	ImageIcon sleepDragon = new ImageIcon("images"+ File.separator +"sleepDragon.gif");
	
	/** The eagle. */
	ImageIcon eagle = new ImageIcon("eagle.gif");
	
	/** The sword. */
	ImageIcon sword = new ImageIcon("images"+ File.separator +"sword.png");
	
	/** The exit. */
	ImageIcon exit = new ImageIcon("images"+ File.separator +"door.JPG");
	
	/** The dead hero. */
	ImageIcon deadHero = new ImageIcon("images"+ File.separator +"dead_link.png");
	
	/** The hero sword. */
	ImageIcon heroSword = new ImageIcon("images"+ File.separator +"armado.png");
	
	/** The white. */
	ImageIcon white = new ImageIcon("images"+ File.separator +"square.jpg");

	/** The sword1. */
	boolean exit1=false,hero1=false,sword1 = false;
	
	/** The dimension. */
	static int dimension = 10;
	
	/** The random. */
	static int random = Tabuleiro.DEFAULT;
	
	/** The num dragons. */
	static int numDragons = 1;
	
	/** The type. */
	static int type = 3;

	/**
	 * JFrame constructor
	 * Generates the JFrame that allows the user to create a personalized maze.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public CreateMaze() {
		setBounds(100, 100, 631, 521);
		panel = new JPanel();
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				int x = arg0.getX();
				int y = arg0.getY();
				Color cor = getCor();
				int i=0;
				for(JPanel jp: panels) {
					if(cor == Color.cyan){
						hero1=true;
						if(jp.getBackground()==cor){
							tab[i/tab.length][i%tab.length] = 'X';
							jp.setBackground(Color.black);
						}
					} else if(cor == Color.green) {
						sword1=true;
						if(jp.getBackground()==cor){
							tab[i/tab.length][i%tab.length] = 'X';
							jp.setBackground(Color.black);
						}
					}
					if(cor == Color.lightGray)
						exit1 = true;
					Rectangle temp = jp.getBounds();
					if(temp.getMinX() < x && temp.getMaxX() >= x){
						if(temp.getMinY() < y && temp.getMaxY()>= y) {
							jp.setBackground(cor);
						}
					}
					i++;
				}
				actualizeTab();
				if(hero1&&sword1&&exit1){
					btnDone.setToolTipText("All needed elements were added correctly");
				}
			}
		});

		panel_1 = new JPanel();

		panel_2 = new JPanel();
		
		btnDone = new JButton("Done");
		btnDone.setToolTipText("There are some essential elements missing");
		btnDone.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				for(JPanel panel: panels){
					if(panel.getBackground()==Color.LIGHT_GRAY)
						exit1=true;
					if(panel.getBackground()==Color.green)
						sword1=true;
					if(panel.getBackground()==Color.cyan)
						hero1=true;
					if(hero1 && sword1 && exit1)
						break;
				}
				if(!(hero1&&sword1&&exit1)){
					return;
				}
				try {
					new GUI(tab).setVisible(true);
					dispose();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		JPanel panel_3 = new JPanel();
		
		JButton btnMenu = new JButton("Menu");
		btnMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				dispose();
				new MainMenu().setVisible(true);
			}
		});
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 339, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 255, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(panel, GroupLayout.PREFERRED_SIZE, 510, GroupLayout.PREFERRED_SIZE)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(panel_3, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createSequentialGroup()
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(btnMenu)
										.addComponent(btnDone))))))
					.addGap(19))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
						.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(panel_3, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(btnDone)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnMenu))
						.addComponent(panel, GroupLayout.PREFERRED_SIZE, 416, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(21, Short.MAX_VALUE))
		);
		GridBagLayout gbl_panel_3 = new GridBagLayout();
		gbl_panel_3.columnWidths = new int[] {70, 0};
		gbl_panel_3.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
		gbl_panel_3.columnWeights = new double[]{0.0};
		gbl_panel_3.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_3.setLayout(gbl_panel_3);
		
		JLabel lblNewLabel = new JLabel("Wall");
		lblNewLabel.setForeground(Color.BLUE);
		lblNewLabel.setBackground(Color.BLUE);
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		panel_3.add(lblNewLabel, gbc_lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Path");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 1;
		panel_3.add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Dragon");
		lblNewLabel_2.setForeground(Color.RED);
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_2.gridx = 0;
		gbc_lblNewLabel_2.gridy = 2;
		panel_3.add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Sword");
		lblNewLabel_3.setForeground(Color.GREEN);
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_3.gridx = 0;
		gbc_lblNewLabel_3.gridy = 3;
		panel_3.add(lblNewLabel_3, gbc_lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Hero");
		lblNewLabel_4.setForeground(Color.CYAN);
		GridBagConstraints gbc_lblNewLabel_4 = new GridBagConstraints();
		gbc_lblNewLabel_4.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_4.gridx = 0;
		gbc_lblNewLabel_4.gridy = 4;
		panel_3.add(lblNewLabel_4, gbc_lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Exit");
		lblNewLabel_5.setForeground(Color.LIGHT_GRAY);
		GridBagConstraints gbc_lblNewLabel_5 = new GridBagConstraints();
		gbc_lblNewLabel_5.gridx = 0;
		gbc_lblNewLabel_5.gridy = 5;
		panel_3.add(lblNewLabel_5, gbc_lblNewLabel_5);

		JLabel lblMazeDimension = new JLabel("Maze Dimension");
		panel_2.add(lblMazeDimension);

		comboBox_1 = new JComboBox();
		comboBox_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String valueString = (String) comboBox_1.getSelectedItem();
				int value = Integer.valueOf(valueString);
				panel.removeAll();
				panel.setLayout(new GridLayout(value,value));
				tab = new char[value][value];
				panels = new ArrayList<JPanel> ();
				generateTab(value);
				actualizeMainPanel();
				panel.repaint();
			}
		});
		panel_2.add(comboBox_1);
		comboBox_1.addItem("10");
		comboBox_1.addItem("11");
		comboBox_1.addItem("12");
		comboBox_1.addItem("13");
		comboBox_1.addItem("14");
		comboBox_1.addItem("15");
		comboBox_1.addItem("16");
		comboBox_1.addItem("17");
		comboBox_1.addItem("18");
		comboBox_1.addItem("19");
		comboBox_1.addItem("20");
		comboBox_1.addItem("21");
		comboBox_1.addItem("22");
		comboBox_1.addItem("23");
		comboBox_1.addItem("24");
		comboBox_1.addItem("25");
		comboBox_1.addItem("26");
		comboBox_1.addItem("27");
		comboBox_1.addItem("28");
		comboBox_1.addItem("29");
		comboBox_1.addItem("30");

		comboBox = new JComboBox();
		panel_1.add(comboBox);	
		comboBox.addItem("Wall");
		comboBox.addItem("Path");
		comboBox.addItem("Sword");
		comboBox.addItem("Dragon");
		comboBox.addItem("Hero");
		comboBox.addItem("Exit");

		getContentPane().setLayout(groupLayout);
	}
	/**
	 * Checks the selected option by the user and returns the color that matches it.
	 * @return Color
	 */
	protected Color getCor() {
		String option = (String) comboBox.getSelectedItem();
		switch (option) {
		case "Wall":
			return Color.blue;
		case "Path":
			return Color.black;
		case "Dragon":
			return Color.red;
		case "Sword":
			return Color.green;
		case "Hero":
			return Color.cyan;
		case "Exit":
			return Color.LIGHT_GRAY;
		default:
			return null;
		}
	}

	/**
	 * Actualizes the panels existing in the main panel, in order to give the user the proper information of the current board.
	 */
	public void actualizeMainPanel() {
		String valueString = (String) comboBox_1.getSelectedItem();
		int value = Integer.valueOf(valueString);
		panel.removeAll();
		for(int i=0;i<value;i++) {
			for(int j=0;j<value;j++) {
				JPanel jpanel = new JPanel();
				jpanel.setBackground(Color.yellow);	
				panel.add(jpanel);
				panels.add(jpanel);
			}
		}
		panel.validate();
	}

	/**
	 * Function that is called on the click of the mouse, actualizing the board.
	 */
	private void actualizeTab() {
		int i=0;
		int resto, quociente;
		for(JPanel jp: panels) {
			resto = i / tab.length;
			quociente = i % tab.length;
			if(jp.getBackground() == Color.black) {
				tab[resto][quociente] = 'X';
			} else if(jp.getBackground() == Color.blue) {
				tab[resto][quociente] = Constants.ParedeRep;
			} else if(jp.getBackground() == Color.black) {
				tab[resto][quociente] = Constants.EspacoRep;
			} else if(jp.getBackground() == Color.green) {
				tab[resto][quociente] = Constants.EspadaRep;
			} else if(jp.getBackground() == Color.red) {
				tab[resto][quociente] = Constants.DragaoRep;
			} else if(jp.getBackground() == Color.cyan) {
				tab[resto][quociente] = Constants.hRepresentacao;
			} else if(jp.getBackground() == Color.LIGHT_GRAY) {
				tab[resto][quociente] = Constants.SaidaRep;
			}
			i++;
		}	
	}

	/**
	 * Generates a board of the chosen size.
	 *
	 * @param value the value
	 */
	public void generateTab(int value) {
		for(int i=0;i<value;i++){
			for(int j=0;j<value;j++){
				tab[i][j] = 'X';
			}
		}
	}
}
