/**
 * @author José Miguel Melo, ei12050; Ricardo Loureiro, ei12034
 */

package maze.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


/**
 * The Class KeyboardOptions.
 * Allows the user to change some keys for their game.
 */
public class KeyboardOptions extends JDialog {

	/** The content panel. */
	private final JPanel contentPanel = new JPanel();
	
	/** The btn new button. */
	private JButton btnNewButton;
	
	/** The btn new button_1. */
	private JButton btnNewButton_1 ;
	
	/** The btn new button_2. */
	private JButton btnNewButton_2 ;
	
	/** The btn new button_3. */
	private JButton btnNewButton_3;
	
	/** The btn new button_4. */
	private JButton btnNewButton_4 ;

	

	/**
	 * Create the dialog.
	 */
	public KeyboardOptions() {
		setTitle("Keyboard Options");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{0, 0, 0, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
		{
			JLabel lblLeft = new JLabel("Left Key");
			GridBagConstraints gbc_lblLeft = new GridBagConstraints();
			gbc_lblLeft.gridwidth = 2;
			gbc_lblLeft.insets = new Insets(0, 0, 5, 5);
			gbc_lblLeft.fill = GridBagConstraints.HORIZONTAL;
			gbc_lblLeft.gridx = 0;
			gbc_lblLeft.gridy = 0;
			contentPanel.add(lblLeft, gbc_lblLeft);
		}
		{
			btnNewButton = new JButton(String.valueOf((char)Game.LEFT_KEY));
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					btnNewButton.setText(" ");
					btnNewButton.addKeyListener(new KeyListener() {
						
						@Override
						public void keyTyped(KeyEvent e) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void keyReleased(KeyEvent e) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void keyPressed(KeyEvent e) {
							// TODO Auto-generated method stub
							btnNewButton.setText(Character.toString(e.getKeyChar()));
							Game.LEFT_KEY = e.getKeyCode();
						}
					});
				}
			});
			
			GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
			gbc_btnNewButton.insets = new Insets(0, 0, 5, 0);
			gbc_btnNewButton.gridx = 2;
			gbc_btnNewButton.gridy = 0;
			contentPanel.add(btnNewButton, gbc_btnNewButton);
		}
		{
			JLabel lblRightKey = new JLabel("Right Key");
			GridBagConstraints gbc_lblRightKey = new GridBagConstraints();
			gbc_lblRightKey.insets = new Insets(0, 0, 5, 5);
			gbc_lblRightKey.gridx = 0;
			gbc_lblRightKey.gridy = 1;
			contentPanel.add(lblRightKey, gbc_lblRightKey);
		}
		{
			btnNewButton_1 = new JButton("New button");
			

			btnNewButton_1 = new JButton(String.valueOf((char)Game.RIGHT_KEY));
			btnNewButton_1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					btnNewButton_1.setText(" ");
					btnNewButton_1.addKeyListener(new KeyListener() {
						
						@Override
						public void keyTyped(KeyEvent e) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void keyReleased(KeyEvent e) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void keyPressed(KeyEvent e) {
							// TODO Auto-generated method stub
							btnNewButton_1.setText(Character.toString(e.getKeyChar()));
							Game.RIGHT_KEY = e.getKeyCode();
						}
					});
				}
			});
			
			
			GridBagConstraints gbc_btnNewButton_1 = new GridBagConstraints();
			gbc_btnNewButton_1.insets = new Insets(0, 0, 5, 0);
			gbc_btnNewButton_1.gridx = 2;
			gbc_btnNewButton_1.gridy = 1;
			contentPanel.add(btnNewButton_1, gbc_btnNewButton_1);
		}
		{
			JLabel lblUpKey = new JLabel("Up Key");
			GridBagConstraints gbc_lblUpKey = new GridBagConstraints();
			gbc_lblUpKey.insets = new Insets(0, 0, 5, 5);
			gbc_lblUpKey.gridx = 0;
			gbc_lblUpKey.gridy = 2;
			contentPanel.add(lblUpKey, gbc_lblUpKey);
		}
		{
			
			btnNewButton_2 = new JButton(String.valueOf((char)Game.UP_KEY));
			btnNewButton_2.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					btnNewButton_2.setText(" ");
					btnNewButton_2.addKeyListener(new KeyListener() {
						
						@Override
						public void keyTyped(KeyEvent e) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void keyReleased(KeyEvent e) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void keyPressed(KeyEvent e) {
							// TODO Auto-generated method stub
							btnNewButton_2.setText(Character.toString(e.getKeyChar()));
							Game.UP_KEY = e.getKeyCode();
						}
					});
				}
			});
			
			GridBagConstraints gbc_btnNewButton_2 = new GridBagConstraints();
			gbc_btnNewButton_2.insets = new Insets(0, 0, 5, 0);
			gbc_btnNewButton_2.gridx = 2;
			gbc_btnNewButton_2.gridy = 2;
			contentPanel.add(btnNewButton_2, gbc_btnNewButton_2);
		}
		{
			JLabel lblDownKey = new JLabel("Down Key");
			GridBagConstraints gbc_lblDownKey = new GridBagConstraints();
			gbc_lblDownKey.insets = new Insets(0, 0, 5, 5);
			gbc_lblDownKey.gridx = 0;
			gbc_lblDownKey.gridy = 3;
			contentPanel.add(lblDownKey, gbc_lblDownKey);
		}
		{
			btnNewButton_3 = new JButton(String.valueOf((char)Game.DOWN_KEY));
			btnNewButton_3.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					btnNewButton_3.setText(" ");
					btnNewButton_3.addKeyListener(new KeyListener() {
						
						@Override
						public void keyTyped(KeyEvent e) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void keyReleased(KeyEvent e) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void keyPressed(KeyEvent e) {
							// TODO Auto-generated method stub
							btnNewButton_3.setText(Character.toString(e.getKeyChar()));

							Game.DOWN_KEY = e.getKeyCode();
						}
					});
				}
			});
			
			GridBagConstraints gbc_btnNewButton_3 = new GridBagConstraints();
			gbc_btnNewButton_3.insets = new Insets(0, 0, 5, 0);
			gbc_btnNewButton_3.gridx = 2;
			gbc_btnNewButton_3.gridy = 3;
			contentPanel.add(btnNewButton_3, gbc_btnNewButton_3);
		}
		{
			JLabel lblEagle = new JLabel("Eagle");
			GridBagConstraints gbc_lblEagle = new GridBagConstraints();
			gbc_lblEagle.insets = new Insets(0, 0, 0, 5);
			gbc_lblEagle.gridx = 0;
			gbc_lblEagle.gridy = 4;
			contentPanel.add(lblEagle, gbc_lblEagle);
		}
		{
			btnNewButton_4 = new JButton(String.valueOf((char)Game.EAGLE_KEY));
			btnNewButton_4.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					btnNewButton_4.setText(" ");
					btnNewButton_4.addKeyListener(new KeyListener() {
						
						@Override
						public void keyTyped(KeyEvent e) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void keyReleased(KeyEvent e) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void keyPressed(KeyEvent e) {
							// TODO Auto-generated method stub
							btnNewButton_4.setText(Character.toString(e.getKeyChar()));
							Game.EAGLE_KEY = e.getKeyCode();
						}
					});
				}
			});
			
			GridBagConstraints gbc_btnNewButton_4 = new GridBagConstraints();
			gbc_btnNewButton_4.gridx = 2;
			gbc_btnNewButton_4.gridy = 4;
			contentPanel.add(btnNewButton_4, gbc_btnNewButton_4);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
					}
				});
				
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
	}

}
