/**
 * @author José Miguel Melo, ei12050; Ricardo Loureiro, ei12034
 */

package maze.gui;

import java.awt.BorderLayout;
import java.awt.Choice;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import maze.logic.Tabuleiro;

// TODO: Auto-generated Javadoc
/**
 * The Class MazeOptions.
 */
public class MazeOptions extends JDialog {

	/** The content panel. */
	private final JPanel contentPanel = new JPanel();
	
	/** The choice. */
	static Choice choice;
	
	/** The choice_1. */
	static Choice choice_1;
	
	/** The choice_2_1. */
	static Choice choice_2_1;
	
	/** The choice_2. */
	static Choice choice_2;
	
	

	/**
	 * Create the dialog.
	 * This dialog allows the user to choose the board size, the type of dragons, generated mazes and how many dragons.
	 */
	public MazeOptions() {
		setResizable(false);
		setTitle("Maze Options");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
		{
		}
		{
			JLabel lblMazeType = new JLabel("Maze Type");
			lblMazeType.setFont(new Font("Lucida Bright", Font.BOLD, 13));
			lblMazeType.setToolTipText("Maze Type");
			GridBagConstraints gbc_lblMazeType = new GridBagConstraints();
			gbc_lblMazeType.fill = GridBagConstraints.HORIZONTAL;
			gbc_lblMazeType.insets = new Insets(0, 0, 5, 5);
			gbc_lblMazeType.gridx = 6;
			gbc_lblMazeType.gridy = 0;
			contentPanel.add(lblMazeType, gbc_lblMazeType);
		}
		{
			choice = new Choice();
			GridBagConstraints gbc_choice = new GridBagConstraints();
			gbc_choice.insets = new Insets(0, 0, 5, 5);
			gbc_choice.gridx = 10;
			gbc_choice.gridy = 0;
			choice.add("Random");
			choice.add("Default");
			contentPanel.add(choice, gbc_choice);
			{
				Label label = new Label("(Default ignores other info)");
				label.setFont(new Font("Avenir", Font.PLAIN, 12));
				GridBagConstraints gbc_label = new GridBagConstraints();
				gbc_label.insets = new Insets(0, 0, 5, 5);
				gbc_label.gridx = 6;
				gbc_label.gridy = 1;
				contentPanel.add(label, gbc_label);
			}
			{
				JLabel lblMazeDimension = new JLabel("Maze Dimension");
				lblMazeDimension.setFont(new Font("Lucida Bright", Font.BOLD, 13));
				lblMazeDimension.setToolTipText("Maze Dimension");
				GridBagConstraints gbc_lblMazeDimension = new GridBagConstraints();
				gbc_lblMazeDimension.fill = GridBagConstraints.HORIZONTAL;
				gbc_lblMazeDimension.insets = new Insets(0, 0, 5, 5);
				gbc_lblMazeDimension.gridx = 6;
				gbc_lblMazeDimension.gridy = 3;
				contentPanel.add(lblMazeDimension, gbc_lblMazeDimension);
			}
			choice_1 = new Choice();
			GridBagConstraints gbc_choice_1 = new GridBagConstraints();
			gbc_choice_1.insets = new Insets(0, 0, 5, 5);
			gbc_choice_1.gridx = 10;
			gbc_choice_1.gridy = 3;
			choice_1.add("10");
			choice_1.add("11");
			choice_1.add("12");
			choice_1.add("13");
			choice_1.add("14");
			choice_1.add("15");
			choice_1.add("16");
			choice_1.add("17");
			choice_1.add("18");
			choice_1.add("19");
			choice_1.add("20");
			choice_1.add("21");
			choice_1.add("22");
			choice_1.add("23");
			choice_1.add("24");
			choice_1.add("25");
			choice_1.add("26");
			choice_1.add("27");
			choice_1.add("28");
			choice_1.add("29");
			choice_1.add("30");
			contentPanel.add(choice_1, gbc_choice_1);
			{
				JLabel lblDragonType = new JLabel("Dragon Type");
				lblDragonType.setFont(new Font("Lucida Bright", Font.BOLD, 13));
				lblDragonType.setToolTipText("Dragon Type");
				GridBagConstraints gbc_lblDragonType = new GridBagConstraints();
				gbc_lblDragonType.fill = GridBagConstraints.HORIZONTAL;
				gbc_lblDragonType.insets = new Insets(0, 0, 5, 5);
				gbc_lblDragonType.gridx = 6;
				gbc_lblDragonType.gridy = 5;
				contentPanel.add(lblDragonType, gbc_lblDragonType);
			}
		}
		{
		}
		{
			choice_2_1 = new Choice();
			GridBagConstraints gbc_choice_2_1 = new GridBagConstraints();
			gbc_choice_2_1.insets = new Insets(0, 0, 5, 5);
			gbc_choice_2_1.gridx = 10;
			gbc_choice_2_1.gridy = 5;
			
						choice_2_1.add("Sleep & Walking");
						choice_2_1.add("Walking");
						choice_2_1.add("Static");
						contentPanel.add(choice_2_1, gbc_choice_2_1);
		}
		choice_2 = new Choice();
		GridBagConstraints gbc_choice_2 = new GridBagConstraints();
		gbc_choice_2.insets = new Insets(0, 0, 0, 5);
		gbc_choice_2.gridx = 10;
		gbc_choice_2.gridy = 7;
		
					choice_2.add("1");
					choice_2.add("2");
					choice_2.add("3");
					choice_2.add("4");
					choice_2.add("5");
					choice_2.add("6");
					choice_2.add("7");
					choice_2.add("8");
					choice_2.add("9");
					choice_2.add("10");
					{
						JLabel lblNumberOfDragons = new JLabel("Number of Dragons");
						lblNumberOfDragons.setFont(new Font("Lucida Bright", Font.BOLD, 13));
						lblNumberOfDragons.setToolTipText("Number of Dragons");
						GridBagConstraints gbc_lblNumberOfDragons = new GridBagConstraints();
						gbc_lblNumberOfDragons.fill = GridBagConstraints.HORIZONTAL;
						gbc_lblNumberOfDragons.insets = new Insets(0, 0, 0, 5);
						gbc_lblNumberOfDragons.gridx = 6;
						gbc_lblNumberOfDragons.gridy = 7;
						contentPanel.add(lblNumberOfDragons, gbc_lblNumberOfDragons);
					}
					contentPanel.add(choice_2, gbc_choice_2);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						String mazeRandom = choice.getSelectedItem();
						if(mazeRandom.equals("Default")){
							Game.random = Tabuleiro.DEFAULT;
						}else{
							Game.random = Tabuleiro.ALEATORIO;
							String mazeDimension = choice_1.getSelectedItem();
							Game.dimension = Integer.valueOf(mazeDimension);
							String numberDragons = choice_2.getSelectedItem();
							Game.numDragons = Integer.valueOf(numberDragons);
							String dragonsType = choice_2_1.getSelectedItem();
							if(dragonsType.equals("Sleep & Walking")){
								Game.type=3;
							}else if(dragonsType.equals("Walking")){
								Game.type=2;
							}else{
								Game.type=1;
							}
						}
						setVisible(false);
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

}
