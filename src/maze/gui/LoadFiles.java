/**
 * @author José Miguel Melo, ei12050; Ricardo Loureiro, ei12034
 */

package maze.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;

import maze.logic.Constants;
import maze.logic.Tabuleiro;
import sprites.Sprite;

/**
 * The Class LoadFiles.
 * This class is based in reading saved files, displaying and allowing the user to play a past saved game.
 */
public class LoadFiles extends JDialog{
	
	/** The panel. */
	JPanel panel;
	
	/** The tab. */
	static char[][] tab;
	
	/** The combo box. */
	JComboBox<String> comboBox;
	
	/** The eagle sprite. */
	Sprite eagleSprite;
	
	
	
	/** The wall. */
	ImageIcon wall = new ImageIcon("images"+ File.separator +"Texture_wall.png");
	
	/** The grass. */
	ImageIcon grass = new ImageIcon("images"+ File.separator +"14.jpg");
	
	/** The hero. */
	ImageIcon hero = new ImageIcon("images"+ File.separator +"unnamed.png");
	
	/** The dragon. */
	ImageIcon dragon = new ImageIcon("images"+ File.separator +"dragon.gif");
	
	/** The sleep dragon. */
	ImageIcon sleepDragon = new ImageIcon("images"+ File.separator +"sleepDragon.gif");
	
	/** The eagle. */
	ImageIcon eagle = new ImageIcon("eagle.gif");
	
	/** The sword. */
	ImageIcon sword = new ImageIcon("images"+ File.separator +"sword.png");
	
	/** The exit. */
	ImageIcon exit = new ImageIcon("images"+ File.separator +"door.JPG");
	
	/** The dead hero. */
	ImageIcon deadHero = new ImageIcon("images"+ File.separator +"dead_link.png");
	
	/** The hero sword. */
	ImageIcon heroSword = new ImageIcon("images"+ File.separator +"armado.png");
	
	/** The white. */
	ImageIcon white = new ImageIcon ("images"+ File.separator +"square.jpg");
	
	/**
	 * Instantiates a new load files.
	 *
	 * @param frame the frame
	 */
	public LoadFiles(final JFrame frame) {
		setResizable(true);
		setTitle("Maze Files");
		setBounds(100, 100, 800, 600);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{379, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);
		
	    comboBox = new JComboBox();
	    comboBox.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		panel.removeAll();
	    		ObjectInputStream is = null; 
	    		try {
					is = new ObjectInputStream( 
							new FileInputStream(new File(".").getCanonicalFile() + File.separator +"Save"+File.separator + comboBox.getSelectedItem()));
				} catch (IOException e1) {
					e1.printStackTrace();
				} 
				try {
					Tabuleiro tabuleiro = (Tabuleiro) is.readObject();
					tab = tabuleiro.getTabuleiro();
				} catch (ClassNotFoundException | IOException e1) {
					e1.printStackTrace();
				} 
				try {
					BufferedImage image = ImageIO.read(new File("images/eagleLeftSprite.png"));
					eagleSprite = new Sprite(image, 4,1);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				panel.setLayout(new GridLayout(tab.length,tab.length));
				for(int i = 0; i < tab.length; i++){
					for(int j = 0; j < tab.length ; j++){
						MazeImage ja = null;
						//PAREDE
						if(tab[i][j] == 'K') {
							ja = new MazeImage(wall.getImage());
							panel.add(ja);
						}
						else if(tab[i][j] == Constants.ParedeRep){
							ja = new MazeImage(wall.getImage());
							panel.add(ja);
						}
						//CAMINHO
						else if(tab[i][j] == Constants.EspacoRep){
							ja = new MazeImage(grass.getImage());
							panel.add(ja);
						}
						//HEROI
						else if(tab[i][j]==Constants.hMorto){
							ja = new MazeImage(deadHero.getImage());
							ja.addBackground(grass.getImage());
							panel.add(ja);
						}	
						else if(tab[i][j] == Constants.hRepresentacao){
							ja = new MazeImage(hero.getImage());
							ja.addBackground(grass.getImage());
							panel.add(ja);
						}else if(tab[i][j] == Constants.AguiaHeroiRep){
							ja = new MazeImage(hero.getImage());
							ja.addBackground(grass.getImage());
							panel.add(ja);
						}
						else if(tab[i][j] == Constants.DragaoRep){
							ja = new MazeImage(dragon.getImage());
							ja.addBackground(grass.getImage());
							panel.add(ja);
						}
						else if(tab[i][j] == Constants.DragaoAdRep){
							ja = new MazeImage(sleepDragon.getImage());
							ja.addBackground(grass.getImage());
							panel.add(ja);
						}
						else if(tab[i][j] == Constants.DragaoEspadaRep){
							ja = new MazeImage(dragon.getImage());
							ja.addBackground(grass.getImage());
							panel.add(ja);
						}
						else if(tab[i][j] == Constants.EspadaRep){
							ja = new MazeImage(sword.getImage());
							ja.addBackground(grass.getImage());
							panel.add(ja);
						}
						else if(tab[i][j] == Constants.SaidaRep){
							ja = new MazeImage(exit.getImage());
							panel.add(ja);
						}
						else if(tab[i][j] == Constants.hArmadoRep){
							ja = new MazeImage(heroSword.getImage());
							ja.addBackground(grass.getImage());
							panel.add(ja);
						}
						else if(tab[i][j] == Constants.AguiaRep || 
								tab[i][j] == Constants.AguiaEspadaRep || 
								tab[i][j] == Constants.AguiaParede){
							if(tab[i][j] == Constants.AguiaParede)
								eagleSprite.addBackground(wall.getImage());
							else
								eagleSprite.addBackground(grass.getImage());
							panel.add(eagleSprite);
						}
					}
				}
				panel.validate();
				panel.repaint();
	    	}
	    });
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.insets = new Insets(0, 0, 5, 0);
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.gridx = 0;
		gbc_comboBox.gridy = 0;
		getContentPane().add(comboBox, gbc_comboBox);
		
		JButton btnLoad = new JButton("LOAD");
		btnLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ObjectInputStream is = null; 
				try { 
					is = new ObjectInputStream( 
							new FileInputStream(new File(".").getCanonicalFile() + File.separator +"Save"+ File.separator + comboBox.getSelectedItem())); 
					Tabuleiro tab = (Tabuleiro) is.readObject(); 
					try {
						setVisible(false);
						new GUI(tab).setVisible(true);
						frame.dispose();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					
				} 
				catch (IOException e1) {

				} catch (Exception e1) {
					e1.printStackTrace();
				} 
				finally { if (is != null)
					try {
						is.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					} } 
			}
		});
		GridBagConstraints gbc_btnLoad = new GridBagConstraints();
		gbc_btnLoad.insets = new Insets(0, 0, 5, 0);
		gbc_btnLoad.gridx = 0;
		gbc_btnLoad.gridy = 1;
		getContentPane().add(btnLoad, gbc_btnLoad);
		
		panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 2;
		getContentPane().add(panel, gbc_panel);
		File save = new File("Save");
		for(int i=0;i<save.listFiles().length;i++) {
			comboBox.addItem(save.listFiles()[i].getName());
		}
	}
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;	

}
