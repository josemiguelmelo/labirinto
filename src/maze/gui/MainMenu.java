/**
 * @author José Miguel Melo, ei12050; Ricardo Loureiro, ei12034
 */

package maze.gui;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;


/**
 * The Class MainMenu.
 * It's the class that will start the game, allows the user to search trhought all the game possibilities.
 */
public class MainMenu extends JFrame {
	
	/** The frame. */
	JFrame frame;
	
	/** The content pane. */
	private JPanel contentPane;

	/**
	 * Launch the application.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainMenu frame = new MainMenu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * There are different options, "The Game", "Play Game", "Keyboard Options", "Exit Game", "Maze Options", "Create Maze" & "Load Files".
	 */
	public MainMenu() {
		frame=this;
		setTitle("Maze : The Game");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 464, 392);
		contentPane = new JPanel();
		contentPane.setBackground(UIManager.getColor("PasswordField.selectionBackground"));
		Image bgImage = new ImageIcon("mainMenuBackground.png").getImage();

		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);


		JButton playButton = new JButton("Play Game");
		playButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					new GUI().setVisible(true);
					dispose();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		JButton keyboardOptionButton = new JButton("Keyboard Options");
		keyboardOptionButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyboardOptions keyboardOption = new KeyboardOptions();
				keyboardOption.setVisible(true);
			}
		});

		JButton exitButton = new JButton("Exit Game");
		exitButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
				System.exit(0);
			}
		});

		JButton mazeOptionButton = new JButton("Maze Options");
		mazeOptionButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MazeOptions mazeOptions  = new MazeOptions();
				mazeOptions.setVisible(true);
			}
		});

		JButton createMazeButton = new JButton("Create Maze");
		createMazeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CreateMaze newMaze = new CreateMaze();
				newMaze.setVisible(true);
				setVisible(false);
			}
		});

		JPanel panel = new JPanel();
		panel.setBackground(UIManager.getColor("PasswordField.selectionBackground"));

		JButton btnNewButton = new JButton("Load Files");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LoadFiles loadFiles = new LoadFiles(frame);
				loadFiles.setVisible(true);
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
				gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPane.createSequentialGroup()
										.addGap(138)
										.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
												.addComponent(playButton, GroupLayout.DEFAULT_SIZE, 169, Short.MAX_VALUE)
												.addComponent(createMazeButton, GroupLayout.DEFAULT_SIZE, 169, Short.MAX_VALUE)
												.addComponent(keyboardOptionButton, GroupLayout.DEFAULT_SIZE, 169, Short.MAX_VALUE)
												.addComponent(mazeOptionButton, GroupLayout.DEFAULT_SIZE, 169, Short.MAX_VALUE)
												.addComponent(exitButton, GroupLayout.DEFAULT_SIZE, 169, Short.MAX_VALUE)
												.addComponent(btnNewButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
												.addGroup(gl_contentPane.createSequentialGroup()
														.addGap(35)
														.addComponent(panel, GroupLayout.DEFAULT_SIZE, 360, Short.MAX_VALUE)))
														.addGap(53))
				);
		gl_contentPane.setVerticalGroup(
				gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
						.addContainerGap()
						.addComponent(panel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(playButton, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(createMazeButton, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(keyboardOptionButton, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(mazeOptionButton, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(exitButton, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
						.addGap(27))
				);

		JLabel lblMazeThe = new JLabel("Maze : The Game");
		lblMazeThe.setFont(new Font("Marker Felt", Font.PLAIN, 32));
		panel.add(lblMazeThe);
		contentPane.setLayout(gl_contentPane);
	}
}
