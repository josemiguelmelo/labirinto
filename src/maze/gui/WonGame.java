/**
 * @author José Miguel Melo, ei12050; Ricardo Loureiro, ei12034
 */

package maze.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 * The Class WonGame.
 *  It's a JDialog that will give the user the possibility to play another game or quit.
 */
public class WonGame extends JDialog {
	
	/** The frame. */
	static JFrame frame;
	
	/** The content panel. */
	private final JPanel contentPanel = new JPanel();

	

	/**
	 * Create the dialog.
	 * @param frame the frame that will be disposed after the game.
	 */
	public WonGame(final JFrame frame) {
		WonGame.frame = frame;
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel lblYouWon = new JLabel("You Won !");
			lblYouWon.setFont(new Font("Herculanum", Font.BOLD, 33));
			lblYouWon.setBounds(130, 72, 252, 68);
			contentPanel.add(lblYouWon);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JLabel lblNewGame = new JLabel("New Game?");
				buttonPane.add(lblNewGame);
			}
			{
				JButton okButton = new JButton("Yes");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						setVisible(false);
						try {
							new GUI().setVisible(true);
							frame.dispose();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			
			JButton btnNo = new JButton("No");
			btnNo.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					setVisible(false);
					new MainMenu().setVisible(true);
					frame.dispose();
				}
			});
			buttonPane.add(btnNo);
		}
	}
}
