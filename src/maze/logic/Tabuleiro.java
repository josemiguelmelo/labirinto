/**
 * @author José Miguel Melo, ei12050; Ricardo Loureiro, ei12034
 */

package maze.logic;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;


// TODO: Auto-generated Javadoc
/**
 * The Class Tabuleiro.
 */
public class Tabuleiro implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8960694670050030335L;
	
	/** The x dimensao. */
	int xDimensao;
	
	/** The y dimensao. */
	int yDimensao;
	
	/** The dragoes. */
	ArrayList<Dragao> dragoes;
	
	/** The h. */
	public Heroi h;
	
	/** The espada. */
	public Espada espada;
	
	/** The a. */
	Aguia a;
	
	/** The rand. */
	public MyRandom rand;

	/** The created. */
	public static int CREATED = 2;
	
	/** The Random Maze. */
	public static int ALEATORIO = 1;
	
	/** The Default Maze. */
	public static int DEFAULT = 0;

	/** The maze. */
	char tabuleiro[][];

	/**
	 * * CONSTRUTORES ***.
	 *
	 * @param dimensao the dimensao
	 */

	Tabuleiro(int dimensao) {
		xDimensao = dimensao;
		yDimensao = dimensao;
		tabuleiro = new char[dimensao][dimensao];
		dragoes = new ArrayList<Dragao>();
		espada = new Espada();
		h = new Heroi();
		a = new Aguia();
		rand = new MyRandom();
	}

	/**
	 * Instantiates a new tabuleiro.
	 */
	Tabuleiro() {
		xDimensao=10;
		yDimensao=10;
		dragoes = new ArrayList<Dragao>(); 
		espada = new Espada();
		h = new Heroi();
		rand = new MyRandom();
	}

	/**
	 * Instantiates a new tabuleiro.
	 *
	 * @param builder the tabuleiro builder
	 */
	Tabuleiro(TabuleiroBuilder builder){
		this.xDimensao = builder.tamanho;
		this.yDimensao = builder.tamanho;
		this.tabuleiro = builder.tabuleiro;
		this.dragoes = builder.dragoes;
		this.espada = builder.espada;
		this.h = builder.h;
		this.a = builder.a;
		this.rand = builder.rand;
	}

	/**
	 * ***********************.
	 *
	 * @return the maze
	 */
	public char[][] getTabuleiro(){
		return tabuleiro;
	}

	/**
	 * Gets the aguia.
	 *
	 * @return the aguia
	 */
	public Aguia getAguia(){
		return a;
	}

	/**
	 * Gets the xDimensao.
	 *
	 * @return the xDimensao
	 */
	public int getXDimensao(){
		return xDimensao;
	}

	/**
	 * Gets the yDimensao.
	 *
	 * @return the yDimensao
	 */
	public int getYDimensao(){
		return yDimensao;
	}

	/**
	 * Gets the dragoes variable.
	 *
	 * @return the dragoes variable
	 */
	public ArrayList<Dragao> getDragoes(){
		return dragoes;
	}

	/**
	 * Updates Board/Maze.
	 */
	public void atualizaTabuleiro(){

		tabuleiro[h.yPosicao][h.xPosicao] = h.representacao;
		if(!a.espada && !h.armado) 
			tabuleiro[espada.yPosicao][espada.xPosicao] = espada.representacao;

		for(Dragao d: dragoes){
			tabuleiro[d.yPosicao][d.xPosicao] = d.representacao;
		}
	}

	/**
	 * Add dragon.
	 *
	 * @param d dragon to add
	 */
	public void inserirDragao(Dragao d) {

		do{
			d.geraPosicao(xDimensao);		
		}while(tabuleiro[d.yPosicao][d.xPosicao] != Constants.EspacoRep || possivel(d)==false);
		tabuleiro[d.yPosicao][d.xPosicao] = d.representacao;
		dragoes.add(d);
	}

	/**
	 * Checks if possible to add dragon D
	 *
	 * @param D the dragon to check
	 * @return true, if successful
	 */
	private boolean possivel(Dragao D) {
		if(D.xPosicao == h.xPosicao && D.yPosicao == h.yPosicao)
			return false;
		else if(D.xPosicao +1 == h.xPosicao && D.yPosicao == h.yPosicao)
			return false;
		else if(D.xPosicao == h.xPosicao && D.yPosicao+1 == h.yPosicao)
			return false;
		else if(D.xPosicao -1 == h.xPosicao && D.yPosicao == h.yPosicao)
			return false;
		else if(D.xPosicao == h.xPosicao && D.yPosicao-1 == h.yPosicao)
			return false;
		else 
			return true;
	}

	/**
	 * Check if possible to move.
	 *
	 * @param direcao the direction to move
	 * @return true, if successful
	 */
	private boolean possivelMove(int direcao) {

		switch(direcao){
		case Constants.DIREITA: {
			if(tabuleiro[h.yPosicao][h.xPosicao + 1] == Constants.EspacoRep || tabuleiro[h.yPosicao][h.xPosicao + 1] == Constants.EspadaRep
					|| tabuleiro[h.yPosicao][h.xPosicao + 1] == Constants.SaidaRep || tabuleiro[h.yPosicao][h.xPosicao +1] == a.representacao
					){
				if(tabuleiro[h.yPosicao][h.xPosicao+1] == Constants.SaidaRep && h.pontos == 0){
					return false;
				} else
					return true;
			}
		}
		break;
		case Constants.ESQUERDA:{
			if(tabuleiro[h.yPosicao][h.xPosicao - 1] == Constants.EspacoRep || tabuleiro[h.yPosicao][h.xPosicao - 1] == Constants.EspadaRep
					||tabuleiro[h.yPosicao][h.xPosicao - 1] == Constants.SaidaRep || tabuleiro[h.yPosicao][h.xPosicao - 1] == a.representacao
					){
				if(tabuleiro[h.yPosicao][h.xPosicao-1] == Constants.SaidaRep && h.pontos == 0){
					return false;
				} else
					return true;
			}
		}
		break;
		case Constants.CIMA: {
			if(tabuleiro[h.yPosicao-1][h.xPosicao] == Constants.EspacoRep || tabuleiro[h.yPosicao-1][h.xPosicao] == Constants.EspadaRep
					|| tabuleiro[h.yPosicao-1][h.xPosicao] == Constants.SaidaRep || tabuleiro[h.yPosicao-1][h.xPosicao] == a.representacao
					){
				if(tabuleiro[h.yPosicao-1][h.xPosicao] == Constants.SaidaRep && h.pontos == 0){
					return false;
				} else
					return true;
			}
		}
		break;
		case Constants.BAIXO: {
			if(tabuleiro[h.yPosicao+1][h.xPosicao] == Constants.EspacoRep || tabuleiro[h.yPosicao+1][h.xPosicao] == Constants.EspadaRep
			|| tabuleiro[h.yPosicao+1][h.xPosicao] == Constants.SaidaRep || tabuleiro[h.yPosicao+1][h.xPosicao] == a.representacao
					){
				if(tabuleiro[h.yPosicao+1][h.xPosicao] == Constants.SaidaRep && h.pontos == 0){
					return false;
				} else
					return true;
			}
		}
			break;
		case Constants.lancaAGUIA:
			if(h.aguia == true)
				return true;
			break;
		}
		return false;
	}

	/**
	 * Changes hero position.
	 *
	 * @param x the x position
	 * @param y the y position
	 */
	private void trocaPosicaoHeroi(int x, int y) {
		tabuleiro[h.yPosicao][h.xPosicao]=Constants.EspacoRep;
		if(h.aguia == false && h.xPosicao == a.xPosicao && h.yPosicao == a.yPosicao ){
			h.representacao = Constants.AguiaHeroiRep;
		} else {
			if(h.aguia)
				h.representacao = Constants.AguiaHeroiRep;
			else
				h.representacao = Constants.hRepresentacao;
		}
		if(h.armado == true) {
			h.representacao = Constants.hArmadoRep;
		}

		if(tabuleiro[y][x] == Constants.EspadaRep ){
			h.representacao=Constants.hArmadoRep;
			h.armado = true;
		}
		if(tabuleiro[y][x]==Constants.AguiaEspadaRep){
			h.representacao=Constants.hArmadoRep;
			h.armado = true;
			tabuleiro[y][x]=h.representacao;
			a.apanhada=true;
		}

		if(tabuleiro[y][x]!=Constants.SaidaRep){
			tabuleiro[y][x]=h.representacao;
			h.yPosicao = y;
			h.xPosicao = x;
		}
		else{
			tabuleiro[h.yPosicao][h.xPosicao]=h.representacao;
		}
		if( h.representacao==Constants.hArmadoRep){
			h.yPosicao = y;
			h.xPosicao = x;
		}


		if(a.xPosicao == h.xPosicao && a.yPosicao == h.yPosicao && !a.poisada && !h.armado)
			tabuleiro[a.yPosicao][a.xPosicao] = Constants.AguiaHeroiRep;

	}

	/**
	 * @param direcao move direction
	 */
	public void move(int direcao) {	
		if(possivelMove(direcao) == true){
			switch(direcao){
			case Constants.DIREITA:
				if(h.aguia==true){
					a.xPosicao=h.xPosicao+1;
				} else {
					if(a.apanhada!=true && !a.morta)
						moverAguia();
				}
				if(possivelMove(direcao))
					trocaPosicaoHeroi(h.xPosicao+1, h.yPosicao);
				break;
			case Constants.ESQUERDA: 	
				if(h.aguia==true){
					a.xPosicao=h.xPosicao-1;
				} else {
					if(a.apanhada!=true && !a.morta)
						moverAguia();
				}
				if(possivelMove(direcao))
					trocaPosicaoHeroi(h.xPosicao-1, h.yPosicao);
				break;
			case Constants.CIMA: 
				if(h.aguia==true){
					a.yPosicao=h.yPosicao-1;
				} else {
					if(a.apanhada!=true && !a.morta)
						moverAguia();
				}
				if(possivelMove(direcao))
					trocaPosicaoHeroi(h.xPosicao, h.yPosicao-1);	
				break;
			case Constants.BAIXO: 	
				if(h.aguia==true){
					a.yPosicao=h.yPosicao+1;
				} else {
					if(a.apanhada!=true && !a.morta)
						moverAguia();
				}
				if(possivelMove(direcao))
					trocaPosicaoHeroi(h.xPosicao, h.yPosicao+1);
				break;
			case Constants.lancaAGUIA:
				a.escolherCaminho(espada);	
				h.aguia = false;
				if(h.armado==true){
					h.representacao = Constants.hArmadoRep;
				} else {
					h.representacao = Constants.hRepresentacao;
				}

				break;
			}
			h.numMovimentos++;
		}
	}

	/**
	 * Checks if game ended.
	 *
	 * @return true, if successful
	 */
	public boolean fimJogo() {
		if(h.morto) {
			return true;
		}
		if(h.pontos == 0){
			return false;
		}
		else return (h.representacao == Constants.hArmadoRep && tabuleiro[h.yPosicao][h.xPosicao] == Constants.SaidaRep);

	}

	/**
	 * Checks if exist dragon nearby.
	 *
	 * @return true, if successful
	 */
	public boolean verificaDragao() {
		for(Dragao d: dragoes){
			if(d.dormir)
				return false;

			if(h.xPosicao + 1 == d.xPosicao && h.yPosicao == d.yPosicao)
				return true;
			else if (h.xPosicao - 1 == d.xPosicao && h.yPosicao == d.yPosicao)
				return true;
			else if(h.yPosicao + 1 == d.yPosicao && h.xPosicao == d.xPosicao)
				return true;
			else if(h.yPosicao - 1 == d.yPosicao && h.xPosicao == d.xPosicao)
				return true;
		}


		return false;
	}

	/**
	 * Checks if hero dies.
	 *
	 * @return true, if successful
	 */
	public boolean morre() {
		if(!verificaDragao())
			return false;
		if(h.representacao==Constants.hRepresentacao || h.representacao ==Constants.AguiaHeroiRep ||
				h.representacao == Constants.hMorto){
			tabuleiro[h.yPosicao][h.xPosicao]=Constants.EspacoRep;
			// se quisermos ter mais "vidas" acrescentar aqui a posicao para onde vai o heroi novamente
			h.morto=true;
			h.representacao=Constants.hMorto;
			tabuleiro[h.yPosicao][h.xPosicao]=h.representacao;

			return true;
		}
		return false;
	}

	/**
	 * Kill dragon.
	 */
	public void mataDragao() {
		Dragao morto = new Dragao();
		if(!h.armado)
			return;
		for(Dragao d: dragoes){
			if(h.xPosicao + 1 == d.xPosicao && h.yPosicao == d.yPosicao){
				tabuleiro[d.yPosicao][d.xPosicao] = Constants.EspacoRep;
				morto=d;
			}
			else if (h.xPosicao - 1 == d.xPosicao && h.yPosicao == d.yPosicao){
				tabuleiro[d.yPosicao][d.xPosicao] = Constants.EspacoRep;
				morto = d;
			}
			else if(h.yPosicao + 1 == d.yPosicao && h.xPosicao == d.xPosicao){
				tabuleiro[d.yPosicao][d.xPosicao] = Constants.EspacoRep;
				morto = d;
			}
			else if(h.yPosicao - 1 == d.yPosicao && h.xPosicao == d.xPosicao){
				tabuleiro[d.yPosicao][d.xPosicao] = Constants.EspacoRep;
				morto=d;
			}
		}
		dragoes.remove(morto);
		h.pontos++;
	}

	/**
	 * Generate random direction.
	 *
	 * @return the random direction generated
	 */
	private int gerarDirecaoAleatoria() {
		int direcaoAleatoria;
		if(rand.isRandom)
			direcaoAleatoria = rand.nextInt(5)+1;
		else
			direcaoAleatoria = rand.nextInt(5);
		return direcaoAleatoria;
	}

	/**
	 * Sleep dragons randomly.
	 */
	public void dragaoDormir(){
		Random rand = new Random();
		for(Dragao d: dragoes){
			if(d.espada)
				continue;
			if(rand.nextInt(5) < 1){
				d.dormir=true;
				d.representacao = Constants.DragaoAdRep;
			}
			else{
				d.dormir=false;
				d.representacao =  Constants.DragaoRep;
			}
		}
	}

	/**
	 * Move dragon.
	 */
	public void moverDragao() {
		for(Dragao d: dragoes){
			if(d.mover && !d.dormir){
				int direcao = gerarDirecaoAleatoria();
				switch(direcao){
				case Constants.DIREITA:
					if(tabuleiro[d.yPosicao][d.xPosicao + 1] == Constants.EspacoRep || 
					tabuleiro[d.yPosicao][d.xPosicao + 1] == Constants.EspadaRep){
						if(tabuleiro[d.yPosicao][d.xPosicao + 1] == Constants.EspadaRep){
							d.representacao=Constants.DragaoEspadaRep;
							d.espada = true;
							tabuleiro[d.yPosicao][d.xPosicao]=Constants.EspacoRep ;
						}
						else{
							if(d.representacao == Constants.DragaoEspadaRep){
								tabuleiro[d.yPosicao][d.xPosicao]=Constants.EspadaRep;
							}else{
								tabuleiro[d.yPosicao][d.xPosicao]=Constants.EspacoRep ;
							}
							d.representacao = Constants.DragaoRep;
						}
						d.xPosicao++;
						tabuleiro[d.yPosicao][d.xPosicao]=d.representacao;
					}
					break;
				case Constants.ESQUERDA:
					if(tabuleiro[d.yPosicao][d.xPosicao - 1] == Constants.EspacoRep  || 
					tabuleiro[d.yPosicao][d.xPosicao - 1] == Constants.EspadaRep){
						if(tabuleiro[d.yPosicao][d.xPosicao - 1] == Constants.EspadaRep){
							d.representacao=Constants.DragaoEspadaRep;
							d.espada = true;
							tabuleiro[d.yPosicao][d.xPosicao]=Constants.EspacoRep ;
						}
						else{
							if(d.representacao == Constants.DragaoEspadaRep){
								tabuleiro[d.yPosicao][d.xPosicao]=Constants.EspadaRep;
							}else{
								tabuleiro[d.yPosicao][d.xPosicao]=Constants.EspacoRep ;
							}
							d.representacao = Constants.DragaoRep;
						}
						d.xPosicao--;
						tabuleiro[d.yPosicao][d.xPosicao]=d.representacao;
					}
					break;
				case Constants.CIMA:
					if(tabuleiro[d.yPosicao-1][d.xPosicao] == Constants.EspacoRep  || 
					tabuleiro[d.yPosicao-1][d.xPosicao] == espada.representacao){
						if(tabuleiro[d.yPosicao-1][d.xPosicao] == espada.representacao){
							d.representacao=Constants.DragaoEspadaRep;
							d.espada = true;
							tabuleiro[d.yPosicao][d.xPosicao]=Constants.EspacoRep ;
						}
						else{
							if(d.representacao == Constants.DragaoEspadaRep){
								tabuleiro[d.yPosicao][d.xPosicao]=espada.representacao;
							}else{
								tabuleiro[d.yPosicao][d.xPosicao]=Constants.EspacoRep ;
							}
							d.representacao = Constants.DragaoRep;
						}
						d.yPosicao--;
						tabuleiro[d.yPosicao][d.xPosicao]=d.representacao;
					}
					break;
				case Constants.BAIXO:
					if(tabuleiro[d.yPosicao+1][d.xPosicao] == Constants.EspacoRep  || 
					tabuleiro[d.yPosicao+1][d.xPosicao] == espada.representacao){
						if(tabuleiro[d.yPosicao+1][d.xPosicao] == espada.representacao){
							d.representacao=Constants.DragaoEspadaRep;
							d.espada = true;
							tabuleiro[d.yPosicao][d.xPosicao]=Constants.EspacoRep ;
						}
						else{
							if(d.representacao == Constants.DragaoEspadaRep){
								tabuleiro[d.yPosicao][d.xPosicao]=espada.representacao;
							}else{
								tabuleiro[d.yPosicao][d.xPosicao]=Constants.EspacoRep ;
							}
							d.representacao = Constants.DragaoRep;
						}
						d.yPosicao++;
						tabuleiro[d.yPosicao][d.xPosicao]=d.representacao;
					}
					break;
				}
				tabuleiro[d.yPosicao][d.xPosicao]=d.representacao;
			}
			if(d.dormir)
				tabuleiro[d.yPosicao][d.xPosicao] = d.representacao;
		}
	}

	/**
	 * Checks if eagle dies.
	 *
	 * @return true, if successful
	 */
	private boolean aguiaMorre(){
		for(Dragao d: dragoes){
			if(d.dormir)
				return false;

			if(a.xPosicao + 1 == d.xPosicao && a.yPosicao == d.yPosicao)
				return true;
			else if (a.xPosicao - 1 == d.xPosicao && a.yPosicao == d.yPosicao)
				return true;
			else if(a.yPosicao + 1 == d.yPosicao && a.xPosicao == d.xPosicao)
				return true;
			else if(a.yPosicao - 1 == d.yPosicao && a.xPosicao == d.xPosicao)
				return true;
			else if(a.yPosicao == d.yPosicao && a.xPosicao == d.xPosicao)
				return true;
		}
		return false;
	}

	/**
	 * Move eagle.
	 */
	public void moverAguia() {
		
		tabuleiro[a.yPosicao][a.xPosicao] = a.ultRep;
		a.poisada=false;		
		a.atualizaPosicao();
		if(tabuleiro[a.yPosicao][a.xPosicao]==Constants.EspadaRep){
			tabuleiro[a.yPosicao][a.xPosicao] = Constants.EspacoRep;
			a.representacao = Constants.AguiaEspadaRep;
			a.espada=true;

		}
		a.ultRep = tabuleiro[a.yPosicao][a.xPosicao];
		if(a.ultRep == Constants.ParedeRep){
			a.representacao = Constants.AguiaParede;
		} else if(a.ultRep == Constants.EspacoRep){
			if(a.espada){
				a.representacao = Constants.AguiaEspadaRep;
			} else {
				a.representacao = Constants.AguiaRep;
			}
		}


		tabuleiro[a.yPosicao][a.xPosicao] = a.representacao;

		if(a.movimentoAtual < a.caminho.size()-1)
			a.movimentoAtual++;
		else {
			a.poisada=true;
			a.inverterCaminho();
			if(tabuleiro[a.yPosicao+1][a.xPosicao]==Constants.DragaoRep || tabuleiro[a.yPosicao][a.xPosicao+1] == Constants.DragaoRep||
					tabuleiro[a.yPosicao-1][a.xPosicao]==Constants.DragaoRep || tabuleiro[a.yPosicao][a.xPosicao-1] == Constants.DragaoRep){
				a.morta=true;
				tabuleiro[a.yPosicao][a.xPosicao] = Constants.EspadaRep;
			}
		}		
		if(aguiaMorre() && a.poisada)
			a.morta=true;

		if(a.ultRep==h.representacao){
			a.ultRep=Constants.EspacoRep;
		}
		if(a.ultRep==Constants.DragaoAdRep||a.ultRep==Constants.DragaoRep){
			a.ultRep=Constants.EspacoRep;
		}
	}

	/**
	 * The Class TabuleiroBuilder.
	 */
	public static class TabuleiroBuilder{
		
		/** The tamanho. */
		private int tamanho;
		
		/** The aleatorio. */
		private int aleatorio;
		
		/** The tabuleiro. */
		private char[][] tabuleiro;
		
		/** The dragoes. */
		private ArrayList<Dragao> dragoes;
		
		/** The h. */
		private Heroi h;
		
		/** The espada. */
		private Espada espada;
		
		/** The a. */
		private Aguia a;
		
		/** The num dragoes. */
		private int numDragoes;
		
		/** The rand. */
		private MyRandom rand;

		/**
		 * Instantiates a new tabuleiro builder.
		 *
		 * @param al ALEATORIO constant if random maze. DEFAULT constant if default maze
		 * @param tam the size of board
		 * @param numeroDragoes the number of dragons
		 */
		public TabuleiroBuilder(int al, int tam, int numeroDragoes) {
			this.tamanho = tam;
			this.aleatorio = al;
			this.tabuleiro = new char[tamanho][tamanho];
			this.numDragoes = numeroDragoes;
			this.dragoes = new ArrayList<Dragao>();
			this.rand = new MyRandom();
		}

		/**
		 * Instantiates a new tabuleiro builder.
		 */
		public TabuleiroBuilder(){
			this.aleatorio = Tabuleiro.DEFAULT;
			this.dragoes = new ArrayList<Dragao>();
			char[][] tab = {
					{Constants.ParedeRep,Constants.ParedeRep,Constants.ParedeRep,Constants.ParedeRep,Constants.ParedeRep,Constants.ParedeRep,Constants.ParedeRep,Constants.ParedeRep,Constants.ParedeRep,Constants.ParedeRep},
					{Constants.ParedeRep,Constants.EspacoRep,Constants.EspacoRep,Constants.EspacoRep,Constants.EspacoRep,Constants.EspacoRep,Constants.EspacoRep,Constants.EspacoRep,Constants.EspacoRep,Constants.ParedeRep},
					{Constants.ParedeRep,Constants.EspacoRep,Constants.ParedeRep,Constants.ParedeRep,Constants.EspacoRep,Constants.ParedeRep,Constants.EspacoRep,Constants.ParedeRep,Constants.EspacoRep,Constants.ParedeRep},
					{Constants.ParedeRep,Constants.EspacoRep,Constants.ParedeRep,Constants.ParedeRep,Constants.EspacoRep,Constants.ParedeRep,Constants.EspacoRep,Constants.ParedeRep,Constants.EspacoRep,Constants.ParedeRep},
					{Constants.ParedeRep,Constants.EspacoRep,Constants.ParedeRep,Constants.ParedeRep,Constants.EspacoRep,Constants.ParedeRep,Constants.EspacoRep,Constants.ParedeRep,Constants.EspacoRep,Constants.ParedeRep},
					{Constants.ParedeRep,Constants.EspacoRep,Constants.EspacoRep,Constants.EspacoRep,Constants.EspacoRep,Constants.EspacoRep,Constants.EspacoRep,Constants.ParedeRep,Constants.EspacoRep,Constants.SaidaRep},
					{Constants.ParedeRep,Constants.EspacoRep,Constants.ParedeRep,Constants.ParedeRep,Constants.EspacoRep,Constants.ParedeRep,Constants.EspacoRep,Constants.ParedeRep,Constants.EspacoRep,Constants.ParedeRep},
					{Constants.ParedeRep,Constants.EspacoRep,Constants.ParedeRep,Constants.ParedeRep,Constants.EspacoRep,Constants.ParedeRep,Constants.EspacoRep,Constants.ParedeRep,Constants.EspacoRep,Constants.ParedeRep},
					{Constants.ParedeRep,Constants.EspacoRep,Constants.ParedeRep,Constants.ParedeRep,Constants.EspacoRep,Constants.EspacoRep,Constants.EspacoRep,Constants.EspacoRep,Constants.EspacoRep,Constants.ParedeRep},
					{Constants.ParedeRep,Constants.ParedeRep,Constants.ParedeRep,Constants.ParedeRep,Constants.ParedeRep,Constants.ParedeRep,Constants.ParedeRep,Constants.ParedeRep,Constants.ParedeRep,Constants.ParedeRep}
			};
			tabuleiro=tab;
			this.tamanho = tabuleiro.length;
			this.rand = new MyRandom();
		}

		/**
		 * Instantiates a new tabuleiro builder.
		 *
		 * @param tab the maze board to set
		 */
		public TabuleiroBuilder(char[][]tab){
			this.aleatorio = Tabuleiro.CREATED;
			int x=0,y=0;
			for(int i=0;i<tab.length;i++){
				for(int j=0;j<tab.length;j++){
					if(tab[i][j] == Constants.hRepresentacao || tab[i][j] == 'T'){
						x=j;y=i;
						break;
					}
				}
			}
			this.h = new Heroi(x,y);
			this.a = new Aguia(x,y);

			for(int i=0;i<tab.length;i++){
				for(int j=0;j<tab.length;j++){
					if(tab[i][j]==Constants.EspadaRep) {
						x=j;y=i;
						break;
					}
				}
			}
			this.espada = new Espada(x,y);

			ArrayList<Dragao> dragoes = new ArrayList<Dragao>();
			for(int i=0;i<tab.length;i++) {
				for(int j=0;j<tab.length;j++) {
					if(tab[i][j] == Constants.DragaoRep){
						Dragao d = new Dragao(j,i,false,true);
						dragoes.add(d);
					}
				}
			}
			this.dragoes = dragoes;
			this.tabuleiro = tab;
			this.rand = new MyRandom();
		}

		/**
		 * @param tam maze size
		 * @return the tabuleiro builder
		 */
		public TabuleiroBuilder tamanho(int tam) {
			this.tamanho = tam;
			return this;
		}

		/**
		 * Random.
		 *
		 * @param al Variable with Random or Default
		 * @return the tabuleiro builder
		 */
		public TabuleiroBuilder aleatorio(int al){
			this.aleatorio = al;
			return this;
		}

		/**
		 * @param tabuleiro the mazeboard
		 * @return the tabuleiro builder
		 */
		public TabuleiroBuilder tabuleiro(char[][] tabuleiro){
			this.tabuleiro = tabuleiro;
			return this;
		}

		/**
		 * Add dragons to maze.
		 *
		 * @param drag the dragons list
		 * @return the tabuleiro builder
		 */
		public TabuleiroBuilder dragoes(ArrayList<Dragao> drag){
			this.dragoes = drag;
			return this;
		}

		/**
		 * Add hero to maze.
		 *
		 * @param h the hero
		 * @return the tabuleiro builder
		 */
		public TabuleiroBuilder heroi(Heroi h){
			this.h = h;
			return this;
		}

		/**
		 * Add sword to maze.
		 *
		 * @param e the sword
		 * @return the tabuleiro builder
		 */
		public TabuleiroBuilder espada(Espada e){
			this.espada = e;
			return this;
		}

		/**
		 * Add eagle to maze.
		 *
		 * @param a the eagle
		 * @return the tabuleiro builder
		 */
		public TabuleiroBuilder aguia(Aguia a){
			this.a = a;
			return this;
		}

		/**
		 * Set number of dragons
		 * 
		 * @param numeroDragoes the number of dragons to add
		 * @return the tabuleiro builder
		 */
		public TabuleiroBuilder numDragoes(int numeroDragoes){
			this.numDragoes = numeroDragoes;
			return this;
		}

		/**
		 * Random.
		 *
		 * @param rand the rand
		 * @return the tabuleiro builder
		 */
		public TabuleiroBuilder random(MyRandom rand){
			this.rand = rand;
			return this;
		}

		/**
		 * Generate board.
		 */
		public void gerarTabuleiro() {
			if(aleatorio == CREATED) {
				return;
			}
			if(aleatorio == DEFAULT){
				tabuleiro[1][1]=Constants.AguiaHeroiRep;
				a = new Aguia(1,1);
				h = new Heroi(1,1);
				Dragao d = new Dragao(3,5, false, true);
				tabuleiro[d.yPosicao][d.xPosicao] = d.representacao;
				dragoes.add(d);
				espada= new Espada(1,8);
				tabuleiro[espada.yPosicao][espada.xPosicao] = espada.representacao;
				h.representacao = Constants.AguiaHeroiRep;
				return;
			}
			// gerar aleatoriamente
			Random rand = new Random();
			int yDimensao = tamanho;
			int xDimensao = tamanho;
			tabuleiro=new char[yDimensao][xDimensao];


			for(int i = 0; i < tabuleiro.length; i++){
				for(int j = 0; j < tabuleiro.length;j++){
					tabuleiro[i][j]=Constants.ParedeRep;
				}
			}

			ArrayList<Point> neighb = new ArrayList<Point>();
			ArrayList<Point> aux = new ArrayList<Point>();

			// gera a posicao inicial, corresponde a saida
			int iniX, iniY;
			int type= rand.nextInt(2);
			if(type==0){
				iniX= rand.nextInt(xDimensao-2)+1;
				iniY = rand.nextInt(2);
				if(iniY==1)
					iniY=yDimensao-1;
			}else{
				iniY= rand.nextInt(yDimensao-2)+1;
				iniX = rand.nextInt(2);
				if(iniX==1)
					iniX=xDimensao-1;
			}
			// __________________________

			Point start = new Point(iniX , iniY, null);	// ponto inicial
			Point oposto;
			neighb = start.side(tabuleiro);	// arrayList com todos os vizinhos que sao paredes

			tabuleiro[start.y][start.x] = Constants.SaidaRep;	// coloca na posicao inicial a saida
			Point ultimo= null;	// ponto que corresponde ao heroi, colocado no final


			try{
				// enquanto o arraylist com as paredes vizinhas nao estiver vazio
				while(!neighb.isEmpty()){
					start = neighb.remove(rand.nextInt(neighb.size()));	// start e uma das paredes que esta contida no arrayList
					oposto=start.opposite();

					if(tabuleiro[start.y][start.x]==Constants.ParedeRep && start.y>0 && start.x>0 && start.x < xDimensao-1 && start.y < yDimensao-1){
						if(tabuleiro[oposto.y][oposto.x] ==Constants.ParedeRep){
							tabuleiro[start.y][start.x] = Constants.EspacoRep; tabuleiro[oposto.y][oposto.x]=Constants.EspacoRep;	// coloca o oposto e o atual como caminho

							ultimo = start;	// artabuleirona o ultimo espaco colocado, q vai corresponder ao heroi

							aux = oposto.side(tabuleiro); // coloca no arrayList os vizinhos que sao paredes do ponto oposto
							for(int i = 0 ; i < aux.size(); i++){
								neighb.add(aux.get(i));	// adiciona os pontos ao arrayList com os vizinhos
							}
						}
					}
				}
			}
			catch(Exception e){
				System.err.println("Exception found " + e);
			}tabuleiro[ultimo.y][ultimo.x]=Constants.AguiaHeroiRep;
			a = new Aguia(ultimo.x,ultimo.y);
			h = new Heroi(ultimo.x,ultimo.y);

			h.representacao = Constants.AguiaHeroiRep;

			bordersInsert();
		}

		/**
		 * Borders insert.
		 */
		private void bordersInsert(){
			for(int i = 0 ; i < tabuleiro.length; i++){
				for(int j =0; j < tabuleiro.length; j++){
					if(i==0 && tabuleiro[i][j] == Constants.EspacoRep){
						tabuleiro[i][j]=Constants.ParedeRep;
					}	
					if(i==tabuleiro.length-1 && tabuleiro[i][j] == Constants.EspacoRep){
						tabuleiro[i][j]=Constants.ParedeRep;
					}
					if(j==tabuleiro.length-1 && tabuleiro[i][j] == Constants.EspacoRep){
						tabuleiro[i][j]=Constants.ParedeRep;
					}
					if(j==0 && tabuleiro[i][j] == Constants.EspacoRep){
						tabuleiro[i][j]=Constants.ParedeRep;
					}
				}
			}
		}

		/**
		 * Add eagle.
		 *
		 * @param a the eagle
		 */
		public void inserirAguia(Aguia a) {
			tabuleiro[a.yPosicao][a.xPosicao] = Constants.AguiaHeroiRep;
			this.a = a;
		}

		/**
		 * Add hero.
		 *
		 * @param h the hero
		 */
		public void inserirHeroi(Heroi h) {
			tabuleiro[h.yPosicao][h.xPosicao] = h.representacao;
			this.h = h;
		}

		/**
		 * Add dragon.
		 *
		 * @param d the dragon
		 */
		public void inserirDragao(Dragao d) {
			do{
				d.geraPosicao(tamanho);		
			}while(tabuleiro[d.yPosicao][d.xPosicao] != Constants.EspacoRep || possivel(d)==false);
			tabuleiro[d.yPosicao][d.xPosicao] = d.representacao;
			dragoes.add(d);
		}

		/**
		 * Checks if possible to add dragon D.
		 *
		 * @param D the dragon to add
		 * @return true, if successful
		 */
		private boolean possivel(Dragao D) {
			if(D.xPosicao == h.xPosicao && D.yPosicao == h.yPosicao)
				return false;
			else if(D.xPosicao +1 == h.xPosicao && D.yPosicao == h.yPosicao)
				return false;
			else if(D.xPosicao == h.xPosicao && D.yPosicao+1 == h.yPosicao)
				return false;
			else if(D.xPosicao -1 == h.xPosicao && D.yPosicao == h.yPosicao)
				return false;
			else if(D.xPosicao == h.xPosicao && D.yPosicao-1 == h.yPosicao)
				return false;
			else 
				return true;
		}

		/**
		 * insert sword.
		 *
		 * @param e the sword
		 */
		public void inserirEspada(Espada e) {
			while(tabuleiro[e.yPosicao][e.xPosicao] != Constants.EspacoRep){
				e.geraPosicao(tamanho);
			}
			tabuleiro[e.yPosicao][e.xPosicao] = e.representacao;
			espada=e;
		}

		/**
		 * Builds the.
		 *
		 * @return the tabuleiro
		 */
		public Tabuleiro build(){
			gerarTabuleiro();
			// insere os dragoes
			if(this.aleatorio==Tabuleiro.ALEATORIO){
				for(int i = 0; i < numDragoes; i++){
					Dragao d = new Dragao();
					inserirDragao(d);
				}
				Espada e = new Espada();
				inserirEspada(e);
			}
			return new Tabuleiro(this);
		}

	}

	/**
	 * Sets the dimension.
	 *
	 * @param currentBoardSize the new dimension
	 */
	@SuppressWarnings("null")
	public void setDimension(int currentBoardSize) {
		char temp[][] = null;
		for(int i=0;i<currentBoardSize;i++) {
			for(int j=0;j<currentBoardSize;j++) {
				temp[i][j] = 'K';
			}
		}
		this.tabuleiro=temp;
	}

};