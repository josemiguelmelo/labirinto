/**
 * @author José Miguel Melo, ei12050; Ricardo Loureiro, ei12034
 */

package maze.logic;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class Heroi.
 */
public class Heroi extends Posicao implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5669768422374161221L;
	
	/** The num movimentos. */
	int numMovimentos = 0;
	
	/** Is hero dead. */
	boolean morto = false;
	
	/** Number of dragons killed. */
	int pontos = 0; // numero de dragoes que matou
	
	/** Has the eagle. */
	public boolean aguia;
	
	/** Has sword. */
	boolean armado;
	
	/**
	 * Instantiates a new heroi.
	 *
	 * @param x the x
	 * @param y the y
	 */
	public Heroi(int x, int y){
		super(x,y,'H');
		armado = false;
		aguia=true;
	}

	/**
	 * Instantiates a new heroi.
	 */
	public Heroi(){
		super(1,1,'H');
		armado = false;
		aguia=true;
	}

	/**
	 * Returns true if hero is armed
	 *
	 * @return the armado variable
	 */
	public boolean getArmado(){ 
		return armado;
	}

	/**
	 * Returns number of moves.
	 *
	 * @return the numMovimentos variable
	 */
	public int getNumMovimentos(){
		return numMovimentos;
	}
	
	/**
	 * Gets the number of dragons killed.
	 *
	 * @return the pontos
	 */
	public int getPontos(){
		return pontos;
	}
	
	/**
	 * Returns true if hero is dead.
	 *
	 * @return the morto variable
	 */
	public boolean getMorto(){
		return morto;
	}

	/**
	 * Sets the aguia.
	 *
	 * @param aguia the new aguia
	 */
	public void setAguia(boolean aguia){
		this.aguia = aguia;
	}
}
