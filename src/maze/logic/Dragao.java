/**
 * @author José Miguel Melo, ei12050; Ricardo Loureiro, ei12034
 */

package maze.logic;
import java.io.Serializable;
import java.util.Random;


// TODO: Auto-generated Javadoc
/**
 * The Class Dragao.
 */
public class Dragao extends Posicao implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1782734901575049204L;

	/** The rand. */
	Random rand = new Random();
	
	/** Dragon is sleeping boolean. */
	boolean dormir;
	
	/** Dragon is moving boolean. */
	boolean mover;
	
	/** Dragon with sword boolean. */
	boolean espada;
	
	/**
	 * Instantiates a new dragao.
	 *
	 * @param x the x
	 * @param y the y
	 * @param dormir the dormir
	 * @param mover the mover
	 */
	public Dragao(int x , int y, boolean dormir, boolean mover) {
		super(x, y, 'D');
		this.dormir = dormir;
		this.mover = mover;
		this.espada = false;
	}

	/**
	 * Instantiates a new dragao.
	 */
	public Dragao() {
		super(0,0,'D');
		dormir = false;
		mover = true;
		this.espada = false;
	}

	/**
	 * Gera posicao.
	 *
	 * @param limit the limit
	 */
	public void geraPosicao(int limit){
		int xPosicao, yPosicao;
		xPosicao = rand.nextInt(limit);
		yPosicao = rand.nextInt(limit);
		super.setValues(xPosicao,yPosicao,'D');
	}

	/**
	 * Sets the dormir variable.
	 * True if dragon is sleeping. False otherwise
	 *
	 * @param dormir the new dormir
	 */
	public void setDormir(boolean dormir){
		this.dormir = dormir;
		if(this.dormir)
			this.representacao = Constants.DragaoAdRep;
		else
			this.representacao = Constants.DragaoRep;
	}
}
