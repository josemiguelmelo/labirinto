/**
 * @author José Miguel Melo, ei12050; Ricardo Loureiro, ei12034
 */

package maze.logic;

import java.io.Serializable;
import java.util.ArrayList;


// TODO: Auto-generated Javadoc
/**
 * The Class Point.
 */
public class Point implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6066715988568399931L;
	
	/** The x. */
	public int x;
	
	/** The y. */
	public int y;
	
	/** The p. */
	Point p;

	/**
	 * Instantiates a new point.
	 *
	 * @param x the x
	 * @param y the y
	 * @param p the p
	 */
	Point(Integer x, Integer y, Point p){
		this.x = x.intValue();
		this.y = y.intValue();
		this.p = p;
	}

	/**
	 * Instantiates a new point.
	 *
	 * @param x the x
	 * @param y the y
	 * @param p the p
	 */
	public Point(int x, int y, Point p){
		this.x=x; this.y=y;
		this.p = p;
	}

	/**
	 * Get's the current's opposite point.
	 *
	 * @return the opposite point
	 */
	public Point opposite(){
		Integer xInt = new Integer(x);
		Integer yInt = new Integer(y);
		if(xInt.compareTo(new Integer(p.x)) != 0){
			return new Point(x+xInt.compareTo(new Integer(p.x)), y, this);
		}
		if(yInt.compareTo(new Integer(p.y)) != 0){
			return new Point(x, y+yInt.compareTo(new Integer(p.y)), this);
		}
		return null;
	}

	/**
	 * .
	 *
	 * @param tabuleiro the game maze
	 * @return array list with walls near this point
	 */
	public ArrayList<Point> side(char [][] tabuleiro){
		ArrayList<Point> list = new ArrayList<Point>();
		try{
			if(tabuleiro[y+1][x] == Constants.ParedeRep)
				list.add(new Point(x, y+1, this));
		}catch(Exception e){}

		try{
			if(tabuleiro[y-1][x] == Constants.ParedeRep)
				list.add(new Point(x, y-1, this));
		}catch(Exception e){}

		try{
			if(tabuleiro[y][x+1] == Constants.ParedeRep)
				list.add(new Point(x+1, y, this));
		}catch(Exception e){}

		try{
			if(tabuleiro[y][x-1] == Constants.ParedeRep)
				list.add(new Point(x-1, y, this));
		}catch(Exception e){}

		return list;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object p1){
		return this.x == ((Point)p1).x && this.y == ((Point)p1).y;
	}
}
