/**
 * @author José Miguel Melo, ei12050; Ricardo Loureiro, ei12034
 */

package maze.logic;

import java.io.Serializable;
import java.util.Random;

// TODO: Auto-generated Javadoc
/**
 * The Class MyRandom.
 */
public class MyRandom implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7042001532533329541L;
	
	/** The random. */
	Random random;
	
	/** True if random number. */
	public boolean isRandom;
	
	/** Predefined returns by MyRandom.
	 * Used by unit tests */
	public int[] movimentos;
	
	/** The position. */
	int posicao;
	
	/**
	 * Instantiates a new my random.
	 */
	public MyRandom(){
		isRandom=true;
		random = new Random();
	}
	
	/**
	 * Instantiates a new my random.
	 *
	 * @param movimentos the movimentos
	 */
	public MyRandom(int[] movimentos){
		isRandom=false;
		this.movimentos = movimentos;
		this.posicao = 0 ;
	}
	
	/**
	 *	Generates integer from 0-max
	 *
	 * @param max the max random number
	 * @return the random number
	 */
	public int nextInt(int max){
		if(isRandom)
			return random.nextInt(max);
		else{
			return movimentos[this.posicao++];
		}
	}
	
}
