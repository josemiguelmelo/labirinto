/**
 * @author José Miguel Melo, ei12050; Ricardo Loureiro, ei12034
 */

package maze.logic;
import java.io.Serializable;
import java.util.Random;


// TODO: Auto-generated Javadoc
/**
 * The Class Espada.
 */
public class Espada extends Posicao implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5233116249458883332L;
	
	/** The rand. */
	Random rand = new Random();
	
	
	/**
	 * Instantiates a new espada.
	 */
	public Espada(){
		super(0, 0,'E');
	}
	
	/**
	 * Instantiates a new espada.
	 *
	 * @param x the x
	 * @param y the y
	 */
	public Espada(int x, int y) {
		super(x, y, 'E');
	}
	
	/**
	 * Generates sword position randomly.
	 *
	 * @param limit the limit
	 */
	public void geraPosicao(int limit){
		int xPosicao, yPosicao;
		xPosicao = rand.nextInt(limit);
		yPosicao = rand.nextInt(limit);
		super.setValues(xPosicao, yPosicao, 'E');
	}
	
}
