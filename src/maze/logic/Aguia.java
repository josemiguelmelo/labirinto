/**
 * @author José Miguel Melo, ei12050; Ricardo Loureiro, ei12034
 */

package maze.logic;

import java.io.Serializable;
import java.util.ArrayList;

// TODO: Auto-generated Javadoc
/**
 * The Class Aguia.
 */
public class Aguia extends Posicao implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -285318938863527697L;
	
	/** Boolean with information about eagle fly state - flying or not flying. */
	boolean poisada;
	
	/** True if eagle has sword. Not true otherwise */
	boolean espada;
	
	/** The apanhada. */
	boolean apanhada;
	
	/** True if eagle is dead. */
	boolean morta;
	
	/** Inverse move or not. */
	boolean inverso;
	
	/** Actual move. */
	int movimentoAtual;
	
	/** Eagle last representation. */
	char ultRep;
	
	/** Route from hero to sword */
	ArrayList<Point> caminho;
	
	/**
	 * Instantiates a new aguia.
	 *
	 * @param x the x
	 * @param y the y
	 */
	public Aguia(int x , int y){
		super(x, y, Constants.AguiaRep);
		movimentoAtual=1;
		inverso=false;
		ultRep= Constants.EspacoRep;
		apanhada=false;
		movimentoAtual=0;
	}
	
	/**
	 * Instantiates a new aguia.
	 */
	public Aguia(){
		super(1,1,Constants.AguiaRep);
		movimentoAtual=1;
		inverso=false;
		apanhada=false;
		ultRep= Constants.EspacoRep;
		movimentoAtual=0;
	}
	
	/**
	 * Route from sword to eagle's initial position
	 */
	public void inverterCaminho(){
		if(inverso==true){
			return;
		}
		ArrayList<Point> caminhoInvertido = new ArrayList<Point>();
		for(int i=caminho.size()-1;i>=0;i--) {
			caminhoInvertido.add(caminho.get(i));
		}
		
		caminho = caminhoInvertido;
		inverso=true;
		movimentoAtual=0;
	}
	
	/**
	 * Updates position.
	 */
	public void atualizaPosicao() {
		xPosicao = caminho.get(movimentoAtual).x;
		yPosicao = caminho.get(movimentoAtual).y;
	}
	
	/**
	 * Choose route.
	 *
	 * @param espada Sword to get
	 */
	public void escolherCaminho(Espada espada) {
		caminho = calcMelhorCaminho(espada);
	}
	
	/**
	 * Calculates best route to sword.
	 *
	 * @param to destination to calculate route
	 * @return array list with all route coordinates
	 */
	public ArrayList<Point> calcMelhorCaminho(Espada to){
		ArrayList<Point> melhorCaminho = new ArrayList<Point>();
	    boolean coord = false;
	    int x=xPosicao;
	    int y=yPosicao;
	    melhorCaminho.add(new Point(x,y,null));
		while(x != to.xPosicao || y != to.yPosicao){
			if(coord){
				coord=false;
				if( x < to.xPosicao)
					x=x+1;
				else if(x > to.xPosicao)
					x=x-1;
				else
					continue;
				melhorCaminho.add(new Point(x, y, null));
			}else{
				coord=true;
				if( y < to.yPosicao)
					y=y+1;
				else if(y > to.yPosicao)
					y=y-1;
				else
					continue;
				melhorCaminho.add(new Point(x, y, null));
			}
		}
		return melhorCaminho;
	}

	/**
	 * Prints the best route.
	 */
	public void printMelhorCaminho(){
		for(int i= 0; i < caminho.size(); i++){
			System.out.println("x = " + caminho.get(i).x + "  |||   y = " + caminho.get(i).y);
		}
	}

	/**
	 * Returns information about sword possession.
	 *
	 * @return true if eagle has sword. false otherwise
	 */
	public boolean getEspada(){
		return espada;
	}

	/**
	 * Returns information about eagle life.
	 *
	 * @return True if eagle is dead. False if alive.
	 */
	public boolean getMorta(){
		return morta;
	}
	
}
