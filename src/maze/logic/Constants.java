/**
 * @author José Miguel Melo, ei12050; Ricardo Loureiro, ei12034
 */

package maze.logic;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class Constants.
 */
public class Constants implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6145318015520659290L;
	
	/** The Constant DIREITA. */
	public static final int DIREITA = 1;
	
	/** The Constant ESQUERDA. */
	public static final int ESQUERDA = 2;
	
	/** The Constant CIMA. */
	public static final int CIMA = 3;
	
	/** The Constant BAIXO. */
	public static final int BAIXO =  4;
	
	/** The Constant lancaAGUIA. */
	public static final int lancaAGUIA = 5;
	
	/** The Constant AguiaParede Representation. 
	 * Used to represent eagle above wall
	 */
	public static final char AguiaParede = 'U';
	
	/** The Constant hRepresentacao Representation.
	 * Used to represent hero
	 */
	public static final char hRepresentacao = 'H';
	
	/** The Constant hArmadoRep Representation.
	 * Used to represent hero with sword
	 *  */
	public static final char hArmadoRep = 'A';
	
	/** The Constant hMorto Representation. 
	 * Used to represent dead hero
	 * */
	public static final char hMorto = 'Z';
	
	/** The Constant AguiaHeroiRep Representation. 
	 * Used to represent hero with eagle
	 * */
	public static final char AguiaHeroiRep = 'T';
	
	/** The Constant DragaoEspadaRep Representation. 
	 * Used to represent dragon above sword
	 * */
	public static final char DragaoEspadaRep = 'F';
	
	/** The Constant EspadaRep Representation. 
	 * 
	 * Used to represent sword
	 * */
	public static final char EspadaRep = 'E';
	
	/** The Constant AguiaRep Representation.
	 * Used to represent eagle 
	 *  */
	public static final char AguiaRep = 'c';
	
	/** The Constant AguiaEspadaRep Representation.
	 * Used to represent eagle with sword
	 *  */
	public static final char AguiaEspadaRep = 'C';
	
	/** The Constant ParedeRep Representation. 
	 * 
	 * Used to represent wall
	 */
	public static final char ParedeRep = 'X';
	
	/** The Constant EspacoRep Representation.
	 * 
	 * Used to represent path
	 *  */
	public static final char EspacoRep = ' ';
	
	/** The Constant DragaoRep Representation.
	 * Used to represent dragon
	 *  */
	public static final char DragaoRep = 'D';
	
	/** The Constant DragaoAdRep Representation. 
	 * 
	 * Used to represent sleeping dragon
	 * */
	public static final char DragaoAdRep = 'd';
	
	/** The Constant SaidaRep Representation. 
	 * Used to represent exit door / end maze
	 * */
	public static final char SaidaRep = 'S';
}
