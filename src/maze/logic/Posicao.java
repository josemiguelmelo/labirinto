/**
 * @author José Miguel Melo, ei12050; Ricardo Loureiro, ei12034
 */

package maze.logic;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class Posicao.
 */
public class Posicao implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2428298415006602314L;
	
	/** The x position. */
	int xPosicao;
	
	/** The y position. */
	int yPosicao;
	
	/** The representation. */
	public char representacao;
	
	/**
	 * Instantiates a new position.
	 *
	 * @param x the x
	 * @param y the y
	 * @param representacao the representation
	 */
	Posicao(int x, int y, char representacao) {
		this.xPosicao = x;
		this.yPosicao = y;
		this.representacao=representacao;
	}
	
	/**
	 * Sets the values.
	 *
	 * @param x the x
	 * @param y the y
	 * @param rep representation
	 */
	public void setValues(int x, int y, char rep) {
		this.xPosicao = x;
		this.yPosicao = y;
		this.representacao = rep;
	}
	
	/**
	 * Gets the xPosicao.
	 *
	 * @return the xPosicao
	 */
	public int getXPosicao(){ return xPosicao;}
	
	/**
	 * Gets the yPosicao.
	 *
	 * @return the yPosicao
	 */
	public int getYPosicao() { return yPosicao;}
	
}
