/**
 * @author José Miguel Melo, ei12050; Ricardo Loureiro, ei12034
 */

package maze.cli;
import java.util.Scanner;

import javax.swing.JFrame;

import maze.logic.Constants;
import maze.logic.Heroi;
import maze.logic.Tabuleiro;



class InvalidOption extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int opt;
	String errorMsg;
	
	public InvalidOption(int o, String error) {
		opt = o;
		errorMsg = error;
	}

}

/**
 * The Class UserInterface.
 * Portion of functions that easily shows to the user the state of the game or require from the user an input.
 */
public class UserInterface {
	
	/** The scan. */
	static Scanner scan = new Scanner(System.in);
	
	/** The window. */
	static JFrame window = new JFrame();
	
	/**
	 * Gets a direction from the user.
	 *
	 * @return the int
	 */
	public static int recebeDirecao(){
		boolean recebeu = false;
		String direcaoString = "";
		
		do{
			try{
				System.out.println("Use as teclas 'WASD' para mover OU 'e' para usar a aguia: ");
				direcaoString = scan.next();
				direcaoString=direcaoString.toLowerCase();
				if(direcaoString.length()!=1 || (!direcaoString.equals("a") && !direcaoString.equals("w") &&
						!direcaoString.equals("s") && !direcaoString.equals("d") && !direcaoString.equals("e"))){
					throw new InvalidOption(-1, "Tecla Invalida\n");
				}
				recebeu=true;
			}
			catch(InvalidOption opt){
				System.out.println(opt.errorMsg);
			}
		}while(!recebeu);
		
		if(direcaoString.equals("a"))
			return Constants.ESQUERDA;
		else if(direcaoString.equals("d"))
			return Constants.DIREITA;
		else if(direcaoString.equals("w"))
			return Constants.CIMA;
		else if(direcaoString.equals("s"))
			return Constants.BAIXO;
		else if(direcaoString.equals("e"))
			return Constants.lancaAGUIA;
		return 0;
		
	}
	
	/**
	 * Allows the user the possibility to choose betwen generated random maze or predefined.
	 * @return
	 */
	public static int escolheAleatorio() {
		Scanner scan = new Scanner(System.in);
		int opcao = 0;
		boolean ok=false;

		System.out.println("1- Tabuleiro aleatorio");
		System.out.println("2- Tabuleiro predefinido");
		do{
			try{
				System.out.println("Use as teclas 1/2 para escolher: ");
				opcao = scan.nextInt();
				if(opcao > 2 || opcao < 1)
					throw new Exception();
				else
					ok = true;
			}
			catch(Exception o){
				scan.next();
			}
		}while(!ok);
		return opcao;
	}

	/**
	 * Gets from the user a number beetween a defined range.
	 *
	 * @param String is the error message
	 * @param Integer min, is the minimum
	 * @param Integer max, is the maximum
	 * @return Integer that was chosen by the user
	 */
	public static int escolheNumero(String msg, int min, int max){
		int numero = -1;
		boolean recebido = false;
		Scanner scan = new Scanner(System.in);
		
		do{
			try{
				System.out.println(msg);
				numero = scan.nextInt();
				if (numero < min || numero > max)
					System.out.println("Numero invalido\nTente Novamente: ");
				else
					recebido=true;
			}catch(Exception e){
				System.out.println("Invalid Option.\nTente Novamente: ");
				scan.next();
				if (numero < min || numero > max)
					System.out.println("Numero invalido\nTente Novamente: ");
				else
					recebido=true;
			}
		}while(!recebido);
		
		return numero;
	}
	
	/**
	 * Shows the game board
	 *
	 * @param tabuleiro tabuleiro
	 */
	public static void mostraTabuleiro(Tabuleiro tabuleiro){
		tabuleiro.atualizaTabuleiro();
		for(int i = 0 ; i < tabuleiro.getYDimensao(); i++){
			for(int j = 0; j < tabuleiro.getXDimensao(); j++){
				System.out.print(tabuleiro.getTabuleiro()[i][j] + " ");
			}
			System.out.println();
		}
	}
	
	/**
	 * Shows the game board
	 *
	 * @param tabuleiro tab
	 * @param Integer the dimension of the shown tab
	 */
	public static void mostraTabuleiro2(char[][] tabuleiro,int dimensao){
		for(int i = 0 ; i < dimensao; i++){
			for(int j = 0; j < dimensao; j++){
				System.out.print(tabuleiro[i][j] + " ");
			}
			System.out.println();
		}
	}

	/**
	 * Shows some hero information to the user.
	 *
	 * @param Heroi the current hero
	 */
	public static void mostraHeroiPontuacao(Heroi h){
		System.out.println("Numero de Movimentos: " + h.getNumMovimentos());
		System.out.println("Dragoes Mortos: " + h.getPontos());
	}

}

