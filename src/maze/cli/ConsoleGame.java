/**
 * @author José Miguel Melo, ei12050; Ricardo Loureiro, ei12034
 */


package maze.cli;

import maze.logic.Tabuleiro;
import maze.logic.Tabuleiro.TabuleiroBuilder;


/**
 * The Class ConsoleGame.
 * This allows the opportunity to play the game in console mode, 
 */
public class ConsoleGame {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String args[]) {
		Tabuleiro tab = null;
		int numeroDragoes,tamanho = 0;
		int opcao = UserInterface.escolheAleatorio();
		int dragoesTipo = UserInterface.escolheNumero("1. Dragoes Estaticos\n2. Dragoes com movimentacao aleatoria\n3. Dragoes com movimentacao e dormir", 1, 3);
		if(opcao==1){	// se for um tabuleiro aleatorio
			numeroDragoes = UserInterface.escolheNumero("Numero de dragoes: ", 0, 10);
			tamanho = UserInterface.escolheNumero("Tamanho do labirinto: ", 5, 30);
			TabuleiroBuilder builder = new TabuleiroBuilder(Tabuleiro.ALEATORIO, tamanho, numeroDragoes);
			tab = builder.build();


		} else if(opcao == 2) { // se for um tabuleiro default
			TabuleiroBuilder builder = new TabuleiroBuilder();
			tab = builder.build();
		}

		do{
			UserInterface.mostraTabuleiro(tab);
			UserInterface.mostraHeroiPontuacao(tab.h);
			tab.move(UserInterface.recebeDirecao());
			if(dragoesTipo!=1){
				if(dragoesTipo==3)
					tab.dragaoDormir();	// coloca alguns dragoes a dormir
				tab.moverDragao();
			}
			if(tab.verificaDragao() && !tab.morre()){
				tab.mataDragao();
			}

		}while(!tab.fimJogo());
		UserInterface.mostraTabuleiro(tab);
		if(!tab.h.getMorto()){
			System.out.println("YOU WIN!!!");
		}
		 
	}
}
